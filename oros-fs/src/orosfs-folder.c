// SPDX-License-Identifier: AGPL-3.0-or-later

#include "export/orosfs-folder.h"
#include "orosfs.h"
#include "orosfs-symlink.h"
#include "orosfs-mountpoint.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

dirstream ofs_children(folder_handle folder, int mode) {
	struct element_handle *h = rw_handle(folder);
	if ((h->flags & ELEMENT_FLAG_FOLDER) == 0) {
		return NULL;
	}
	struct dirstream_handle *result = malloc(sizeof(struct dirstream_handle));
	result->rw_handle = h;
	result->modCount = h->handlesOrCounts.rcMc.modCount;
	result->current_place.block = 0;
	result->current_place.pos = 0;
	result->child_flags = folder->flags
			| ((uint32_t) (mode & OFS_DESC_MODE_MASK)
					<< (32 - OFS_DESC_MODE_BITS));
	h->handlesOrCounts.rcMc.reference_count++;
	_Static_assert(( //
	/*		*/((uint32_t) (OFS_DESC_MODE_MASK) << (32 - OFS_DESC_MODE_BITS)) //
	/*		*/& ELEMENT_FLAG_MASK //
	) == 0, "Error: element flags and directory stream mode flags overlap!");
	return result;
}

bool ofs_descendant_impl(folder_handle folder, const char *path,
		size_t path_len, int mode, struct orosfs **const restrict fs,
		struct place *const restrict place, uint32_t *const restrict cur_flags,
		unsigned *const restrict link_count) {
	uint64_t block_num = place->block;
	void *block = (*fs)->bm->get((*fs)->bm, block_num);
	while (1) {
		const char *end = memchr(path, '/', path_len);
		if (!end)
			end = path + path_len;
		ptrdiff_t len = end - path;
		path_len -= len;
		cont_same_path_seg: ;
		struct fs_folder *f = block + place->pos;
		uint_fast32_t helper = UINT_FAST32_MAX;
		for (uint_fast32_t i = 0; i < f->direct_children_count; i++) {
			uint32_t n_pos = f->direct_children[i].name_pos;
			uint32_t n_len = obt_get_size(block, (*fs)->bm->block_size, n_pos);
			if ((n_len != len) || memcmp(block + n_pos, path, n_len)) {
				continue;
			}
			const uint32_t cflags = f->direct_children[i].flags;
			set_place(*place, f->direct_children[i].element_place);
			*cur_flags = cflags;
			if (cflags & ELEMENT_FLAG_FOLDER) {
				goto cont_next_seg;
			}
			if (cflags & ELEMENT_FLAG_SYMLINK) {
				if (mode & OFS_DESC_MODE_NO_FOLLOW_SYMLINK) {
					if (path_len) {
						return true;
					}
				} else if (follow_symlink(folder, mode, fs, place, cur_flags,
						link_count)) {
					return true;
				}
				goto cont_next_seg;
			}
			if (cflags & ELEMENT_FLAG_MOUNTPOINT) {
				if (path_len) {
					if (mode & OFS_DESC_MODE_ONE_FILE_SYSTEM) {
						return true;
					}
					if (follow_mountpoint(folder, mode, fs, place, cur_flags,
							link_count)) {
						return true;
					}
				} else if (mode & OFS_DESC_MODE_NO_MOUNT_POINT) {
					return true;
				}
				goto cont_next_seg;
			}
			if (cflags & ELEMENT_FLAG_FILE) {
				if (path_len) {
					return true;
				}
				goto cont_next_seg;
			}
			assert((cflags & ELEMENT_FLAG_TYPE_MASK) == 0);
			assert(helper == UINT_FAST32_MAX);
			helper = i;
		}
		if (helper != UINT_FAST32_MAX) {
			set_place(*place, f->direct_children[helper].element_place);
			helper = UINT32_MAX;
			(*fs)->bm->unget((*fs)->bm, block_num);
			block_num = place->block;
			block = (*fs)->bm->get((*fs)->bm, block_num);
			goto cont_same_path_seg;
		}
		(*fs)->bm->unget((*fs)->bm, block_num);
		return true;
		cont_next_seg: ;
		if (place->block != block_num) {
			(*fs)->bm->unget((*fs)->bm, block_num);
			block_num = place->block;
			block = (*fs)->bm->get((*fs)->bm, block_num);
		}
		path = end;
		if (!*path) {
			return false;
		}
	}
	(*fs)->bm->unget((*fs)->bm, block_num);
	return true;
}

static inline handle ofs_descendant_impl0(folder_handle folder,
		const char *path, int mode, unsigned *restrict link_count) {
	struct orosfs *fs = folder->fs;
	struct place place = folder->place;
	uint32_t cur_flags = folder->flags;
	if (ofs_descendant_impl(folder, path, strlen(path), mode, &fs, &place,
			&cur_flags, link_count)) {
		return NULL;
	}
	struct element_handle result = { .fs = fs, .place =
	/*	*/init_place(place), .flags = cur_flags
			& (folder->flags | ~ELEMENT_HANDLE_FLAG_POTENTIAL_RW) };
	return add_handle(&result);
}

handle ofs_descendant(folder_handle folder, const char *path, int mode) {
	unsigned link_count = 0;
	return ofs_descendant_impl0(folder, path, mode, &link_count);
}

folder_handle ofs_descendant_folder(folder_handle folder, const char *path,
		int mode) {
	unsigned link_count = 0;
	handle res = ofs_descendant_impl0(folder, path, mode, &link_count);
	if (res->flags & ELEMENT_FLAG_FOLDER) {
		return res;
	}
	return NULL;
}

file_handle ofs_descendant_file(folder_handle folder, const char *path,
		int mode) {
	unsigned link_count = 0;
	handle res = ofs_descendant_impl0(folder, path, mode, &link_count);
	if (res->flags & ELEMENT_FLAG_FILE) {
		return res;
	}
	return NULL;
}

symlink_handle ofs_descendant_symlink(folder_handle folder, const char *path,
		int mode) {
	unsigned link_count = 0;
	handle res = ofs_descendant_impl0(folder, path,
			mode | OFS_DESC_MODE_NO_FOLLOW_SYMLINK, &link_count);
	if (res->flags & ELEMENT_FLAG_FOLDER) {
		return res;
	}
	return NULL;
}

mountpoint_handle ofs_descendant_mountpoint(folder_handle folder,
		const char *path, int mode) {
	unsigned link_count = 0;
	handle res = ofs_descendant_impl0(folder, path,
			mode | OFS_DESC_MODE_NO_OPEN_MOUNT_POINT, &link_count);
	if (res->flags & ELEMENT_FLAG_FOLDER) {
		return res;
	}
	return NULL;
}

folder_handle ofs_create_folder(folder_handle folder, const char *path) {

}

file_handle ofs_create_file(folder_handle folder, const char *path) {

}

symlink_handle ofs_create_symlink(folder_handle folder, const char *path,
		const char *target) {

}

mountpoint_handle ofs_create_mountpoint_tmp(folder_handle folder,
		const char *path, bool support_mounts, bool stay_open,
		uint64_t block_count, uint8_t block_size_shift) {

}

mountpoint_handle ofs_create_mountpoint_name(folder_handle folder,
		const char *path, const char *device_name, bool partition_name,
		const char *file_system) {

}

mountpoint_handle ofs_create_mountpoint_uuid(folder_handle folder,
		const char *path, const uuid_t device_uuid, bool partition_uuid,
		const char *file_system) {

}

handle ofs_create_hardlink(folder_handle folder, const char *path,
		handle target) {

}

handle ofs_create_hardlink_path(folder_handle folder, const char *path,
		const char *target) {

}

handle ofs_move(folder_handle folder, const char *dst_path,
		const char *src_path) {

}

handle ofs_remove(folder_handle folder, const char *path) {

}

handle ofs_follow_mount(mountpoint_handle symlink) {

}

void ofs_read_symlink(symlink_handle symlink, char **buf, size_t *buf_len) {

}

size_t ofs_read_symlink_len(symlink_handle symlink) {

}

handle ofs_dirstr_next(dirstream stream, char **name_buf, size_t buf_size,
		uint32_t *flags);
