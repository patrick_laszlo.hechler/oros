/*
 * orosfs-symlink.h
 *
 *  Created on: Apr 25, 2024
 *      Author: pat
 */

#ifndef SRC_OROSFS_MOUNTPOINT_H_
#define SRC_OROSFS_MOUNTPOINT_H_

#include "orosfs.h"

handle mount(mountpoint_handle mount, int mode, unsigned *restrict link_count);

static inline bool follow_mountpoint(folder_handle folder, int mode,
		struct orosfs **restrict fs, struct place *restrict place,
		uint32_t *restrict cur_flags, unsigned *restrict link_count) {
	if (++(*link_count) > OFS_MAX_LINK_COUNT) {
		return true;
	}
	struct element_handle mph = {
			.fs = *fs,
			.place = *place,
			.flags = *cur_flags & ELEMENT_FLAG_MASK
	};
	struct hs_mount_point_entry*mpe = hashset_get(&mount_points, mp_hash(&mph), &mph);
	if (mpe) {
		*fs = mpe->target->fs;
		*place = mpe->target->place;
		*cur_flags = mpe->target->flags;
		return false;
	}
	mountpoint_handle rmph = add_handle(&mph);
	handle result = mount(rmph, mode, link_count);
	if (!result) {
		return true;
	}
	*fs = result->fs;
	*place = result->place;
	*cur_flags = result->flags;
	return false;
}

#endif /* SRC_OROSFS_MOUNTPOINT_H_ */
