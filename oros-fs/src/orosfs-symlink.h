/*
 * orosfs-symlink.h
 *
 *  Created on: Apr 25, 2024
 *      Author: pat
 */

#ifndef SRC_OROSFS_SYMLINK_H_
#define SRC_OROSFS_SYMLINK_H_

#include "orosfs.h"
#include <stddef.h>

static inline bool follow_symlink(folder_handle folder, int mode,
		struct orosfs **restrict fs, struct place *restrict place,
		uint32_t *restrict cur_flags, unsigned *restrict link_count) {
	if (++(*link_count) > OFS_MAX_LINK_COUNT) {
		return true;
	}
	const uint64_t block_num = place->block;
	void *block = (*fs)->bm->get((*fs)->bm, block_num);
	struct fs_symlink *sl = block + place->pos;
	uint32_t path_len = obt_get_size(block, (*fs)->bm->block_size,
			place->pos) - offsetof(struct fs_symlink, path);
	mode &= ~(OFS_DESC_MODE_NO_FOLLOW_SYMLINK
			| OFS_DESC_MODE_NO_OPEN_MOUNT_POINT);
	if (mode & OFS_DESC_MODE_ONE_FILE_SYSTEM) {
		mode |= OFS_DESC_MODE_NO_OPEN_MOUNT_POINT;
	}
	bool result = ofs_descendant_impl(folder, sl->path, path_len, mode, fs,
			place, cur_flags, link_count);
	(*fs)->bm->unget((*fs)->bm, block_num);
	return result;
}

#endif /* SRC_OROSFS_SYMLINK_H_ */
