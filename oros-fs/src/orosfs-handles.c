/*
 * orosfs-handles.c
 *
 *  Created on: Apr 25, 2024
 *      Author: pat
 */

#define OROSFS_HANDLES

#include "orosfs.h"
#include <string.h>

handle add_handle_use(handle uf) {
	uf->handlesOrCounts.rcMc.modCount = 0;
	uf->handlesOrCounts.rcMc.reference_count = 1;
	if (uf->flags & ELEMENT_HANDLE_FLAG_POTENTIAL_RW) {
		struct element_handle *old = hashset_add(&uf->fs->open_handles,
				handle_hash(uf), uf);
		if (old) {
			old->handlesOrCounts.rcMc.reference_count++;
			free(uf);
		}
		return old;
	}
	uf->flags |= ELEMENT_HANDLE_FLAG_POTENTIAL_RW;
	struct element_handle *old = hashset_add(&uf->fs->open_handles,
			handle_hash(uf), uf);
	if (old) {
		old->handlesOrCounts.rcMc.reference_count++;
		uf->flags &= ~ELEMENT_HANDLE_FLAG_POTENTIAL_RW;
		uf->handlesOrCounts.handles.rwHandle = old;
		return uf;
	}
	struct element_handle *res = malloc(sizeof(struct element_handle));
	res->fs = uf->fs;
	res->place = uf->place;
	res->flags = uf->flags & ~ELEMENT_HANDLE_FLAG_POTENTIAL_RW;
	res->handlesOrCounts.handles.rwHandle = uf;
	return res;
}

handle add_handle(handle daa) {
	struct orosfs *fs = daa->fs;
	const uint32_t flags = daa->flags;
	const struct place place = daa->place;
	struct element_handle *old = hashset_get(&fs->open_handles,
			handle_hash(daa), daa);
	if (old) {
		if (flags & ELEMENT_HANDLE_FLAG_POTENTIAL_RW) {
			old->handlesOrCounts.rcMc.reference_count++;
		} else {
			old->handlesOrCounts.handles.rwHandle->handlesOrCounts.rcMc.reference_count++;
		}
		return old;
	}
	if (!(flags & ELEMENT_HANDLE_FLAG_POTENTIAL_RW)) {
		daa->flags |= ELEMENT_HANDLE_FLAG_POTENTIAL_RW;
		old = hashset_get(&fs->open_handles, handle_hash(daa), daa);
		if (old) {
			old->handlesOrCounts.rcMc.reference_count++;
		} else {
			old = malloc(sizeof(struct element_handle));
			old->fs = fs;
			set_place(old->place, place);
			old->flags = flags | ELEMENT_HANDLE_FLAG_POTENTIAL_RW;
			old->handlesOrCounts.rcMc.modCount = 0;
			old->handlesOrCounts.rcMc.reference_count = 1;
			void *old_old = hashset_add(&fs->open_handles, handle_hash(old),
					old);
			if (old_old) {
				free(old);
				old = old_old;
				old->handlesOrCounts.rcMc.reference_count++;
			}
		}
		struct element_handle *res = malloc(sizeof(struct element_handle));
		res->fs = fs;
		set_place(res->place, place);
		res->flags = flags;
		res->handlesOrCounts.handles.rwHandle = old;
		old = hashset_add(&fs->open_handles, handle_hash(res), res);
		if (old) {
			free(res);
			res = old;
		}
		return res;
	}
	struct element_handle *res = malloc(sizeof(struct element_handle));
	res->fs = fs;
	set_place(res->place, place);
	res->flags = flags;
	res->handlesOrCounts.rcMc.modCount = 0;
	res->handlesOrCounts.rcMc.reference_count = 1;
	old = hashset_add(&fs->open_handles, handle_hash(res), res);
	if (old) {
		old->handlesOrCounts.rcMc.reference_count++;
		free(res);
		res = old;
	}
	return res;
}

uint64_t handle_hash(const void *_hash_me) {
	const struct element_handle *hash_me = _hash_me;
	uint64_t result;
	result = 31 + (ptrdiff_t) hash_me->fs->bm;
	result = result * 31 + hash_me->place.block;
	result = result * 31 + hash_me->place.pos;
	result =
			result * 31
					+ (hash_me->flags
							& (ELEMENT_HANDLE_FLAG_POTENTIAL_RW
									| ELEMENT_FLAG_TYPE_MASK));
	return result;
}

bool handle_equal(const void *_a, const void *_b) {
	const struct element_handle *a = _a, *b = _b;
	return a->place.block == b->place.block && a->place.pos == b->place.pos //
			&& (a->flags & ELEMENT_HANDLE_FLAG_POTENTIAL_RW)
					== (b->flags & ELEMENT_HANDLE_FLAG_POTENTIAL_RW) //
			&& a->fs == b->fs;
}

static inline uint64_t str_hash(const char *hash_me) {
	uint64_t result = 1;
	for (; *hash_me; hash_me++) {
		result = result * 31 + *hash_me;
	}
	return result;
}

uint64_t fs_hash(const void *_hash_me) {
	const struct orosfs *hash_me = _hash_me;
	uint64_t result;
	result = 31
			+ (((uint64_t*) hash_me->fs_uuid)[0]
					^ ((uint64_t*) hash_me->fs_uuid)[1]);
	result = result * 31 + str_hash(hash_me->fs_name);
	result = result * 31
			+ (((uint64_t*) hash_me->part_uuid)[0]
					^ ((uint64_t*) hash_me->part_uuid)[1]);
	result = result * 31 + str_hash(hash_me->part_name);
	return result;
}

bool fs_equal(const void *_a, const void *_b) {
	const struct orosfs *a = _a, *b = _b;
	return ((uint64_t*) a->fs_uuid)[0] == ((uint64_t*) b->fs_uuid)[0]
			&& ((uint64_t*) a->fs_uuid)[1] == ((uint64_t*) b->fs_uuid)[1]
			&& ((uint64_t*) a->part_uuid)[0] == ((uint64_t*) b->part_uuid)[0]
			&& ((uint64_t*) a->part_uuid)[1] == ((uint64_t*) b->part_uuid)[1]
			&& strcmp(a->fs_name, b->fs_name)
			&& strcmp(a->part_name, b->part_name);
}

uint64_t mp_hash(const void *hash_me) {
	return handle_hash(((struct hs_mount_point_entry*) hash_me)->mp);
}

bool mp_equal(const void *a, const void *b) {
	return handle_equal(((struct hs_mount_point_entry*) a)->mp,
			((struct hs_mount_point_entry*) b)->mp);
}
