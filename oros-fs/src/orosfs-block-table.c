// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs-block-table.c
 *
 *  Created on: Apr 19, 2024
 *      Author: pat
 */

#include "orosfs-block-table.h"
#include <string.h>
#include <stdlib.h>

/*
 * block data
 * block table:
 *   block table entry*
 *   block table start
 */

struct block_table_entry {
	uint32_t start;
	uint32_t end;
};

static void invalid_block_table(void *block, uint32_t block_size) __attribute__ ((__noreturn__));

bool obt_free(void *block, uint32_t block_size, uint32_t location) { // @suppress("No return")
	struct block_table_entry *entries_high = block + block_size
			- (sizeof(uint32_t) * 3);
	struct block_table_entry *entries_low = block + entries_high[1].start;
	while (entries_high >= entries_low) {
		struct block_table_entry *entries_mid = entries_low
				+ ((entries_high - entries_low) >> 1);
		if (entries_mid->start > location) {
			entries_low = entries_mid - 1;
		} else if (entries_mid->start < location) {
			entries_high = entries_mid + 1;
		} else {
			uintptr_t diffML = (void*) entries_mid - (void*) entries_low;
			memmove(entries_low + 1, entries_low,
					diffML - sizeof(struct block_table_entry));
			uint32_t *table_start = block + block_size - sizeof(uint32_t);
			*table_start += sizeof(struct block_table_entry);
			if (*table_start >= block_size - sizeof(uint32_t)) {
				if (*table_start > block_size - sizeof(uint32_t)) {
					invalid_block_table(block, block_size);
				}
				return true;
			}
			return false;
		}
	}
	invalid_block_table(block, block_size);
}

uint32_t obt_alloc(void *block, uint32_t block_size, uint32_t alloc_size) {
	struct block_table_entry *entries_end = block + block_size
			- sizeof(uint32_t);
	struct block_table_entry *entries = block + entries_end->start;
	if (entries_end->start - entries_end[-1].end
			< sizeof(struct block_table_entry)) {
		return UINT32_MAX;
	}
	struct block_table_entry *most_free = NULL;
	bool first_is_most_free = false;
	uint32_t most_free_space = 0;
	if (entries->start != 0) {
		most_free = entries;
		first_is_most_free = true;
		most_free_space = entries->start;
	}
	while (entries < entries_end) {
		uint32_t free_space = entries[1].start - entries[0].end;
		if (free_space > most_free_space) {
			most_free_space = free_space;
			most_free = entries;
			first_is_most_free = false;
		}
		entries++;
	}
	if (most_free && most_free_space >= alloc_size) {
		uint32_t add_free = most_free_space - alloc_size;
		uint32_t add_loc = add_free >> 1;
		if (first_is_most_free) {
			entries_end->start -= sizeof(struct block_table_entry);
			struct block_table_entry *entry = block + entries_end->start;
			entry->start = 0;
			entry->end = alloc_size;
			return 0;
		} else {
			void *old_start = block + entries_end->start;
			entries_end->start -= sizeof(struct block_table_entry);
			memmove(old_start - sizeof(struct block_table_entry), old_start,
					(void*) most_free - (void*) old_start);
			most_free->start = most_free->end + add_free;
			most_free->end = most_free->start + alloc_size;
			return most_free->start;
		}
	}
	return UINT32_MAX;
}

bool obt_grow_shrink(void *block, uint32_t block_size, uint32_t location, // @suppress("No return")
		uint32_t new_size) {
	struct block_table_entry *entries_high = block + block_size
			- (sizeof(uint32_t) * 3);
	struct block_table_entry *entries_low = block + entries_high[1].start;
	while (entries_high >= entries_low) {
		struct block_table_entry *entries_mid = entries_low
				+ ((entries_high - entries_low) >> 1);
		if (entries_mid->start > location) {
			entries_low = entries_mid - 1;
		} else if (entries_mid->start < location) {
			entries_high = entries_mid + 1;
		} else {
			uint32_t max_new_size = entries_mid[1].start - entries_mid->start;
			if (max_new_size < new_size) {
				return false;
			}
			entries_mid->end = location + new_size;
			return true;
		}
	}
	invalid_block_table(block, block_size);
}

uint32_t obt_get_size(void *block, uint32_t block_size, uint32_t location) { // @suppress("No return")
	struct block_table_entry *entries_high = block + block_size
			- (sizeof(uint32_t) * 3);
	struct block_table_entry *entries_low = block + entries_high[1].start;
	while (entries_high >= entries_low) {
		struct block_table_entry *entries_mid = entries_low
				+ ((entries_high - entries_low) >> 1);
		if (entries_mid->start > location) {
			entries_low = entries_mid - 1;
		} else if (entries_mid->start < location) {
			entries_high = entries_mid + 1;
		} else {
			return entries_mid->end - location;
		}
	}
	invalid_block_table(block, block_size);
}

#include <stdio.h>

static void invalid_block_table(void *block, uint32_t block_size) {
	uint32_t start = *(uint32_t*) (block + block_size - sizeof(uint32_t));
	fprintf(stderr, "invalid block table!\n"
			"block size: %u : 0x%x\n"
			"block table start: %u : 0x%x\n", block_size, block_size, start,
			start);
	abort();
}
