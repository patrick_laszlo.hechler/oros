// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs-defs.h
 *
 *  Created on: Apr 19, 2024
 *      Author: pat
 */

#ifndef SRC_OROSFS_DEFS_H_
#define SRC_OROSFS_DEFS_H_

#include "export/orosfs-defs.h"
#include "export/bm.h"
#include "export/hashset.h"
#include "export/orosfs-constants.h"

#define wait5ms() { \
	struct timespec wait_time = { \
			/*	  */.tv_sec = 0, /* 0 sec */ \
					.tv_nsec = 5000000 /* 5 ms */ \
			}; \
	nanosleep(&wait_time, NULL); \
}

struct orosfs {
	struct bm_block_manager *bm;
	struct hashset open_handles;
	const char *fs_name;
	const uuid_t fs_uuid;
	const char *part_name;
	const uuid_t part_uuid;
};

struct place {
	uint64_t block;
	uint32_t pos;
};

#define set_place(dst, src) \
		(dst).block = (src).block; \
		(dst).pos = (src).pos

#define init_place(src) { \
	.block = (src).block, \
	.pos = (src).pos \
}

#define MAGIC          UINT64_C(2D8857AE8CF514BD)
#define MAGIC_NO_MOUNT UINT64_C(B936BC5C6DC4BBD5)

struct fs_place {
	uint64_t block;
	uint32_t pos;
} __attribute__((packed));

struct super_block {
	uint64_t magic;

	uint64_t block_count;
	uint8_t block_size_shift;

	uint8_t flags[3];

	uint32_t root_pos;
	uint64_t root_block;

	uuid_t uuid;
	uint8_t name[];
} __attribute__((packed));

struct fs_element {
	int64_t hardlink_count;
	int64_t last_mod;
} __attribute__((packed));

struct fs_folder_entry {
	struct fs_place element_place;
	uint32_t name_pos;
	int64_t create_time;
	uint32_t flags;
	uint32_t exec_wrapper_name_pos;
} __attribute__((packed));

struct fs_folder {
	struct fs_element element;
	uint32_t direct_children_count;
	struct fs_folder_entry direct_children[];
} __attribute__((packed));

struct fs_file {
	struct fs_element element;
	uint64_t size;
	uint64_t first_block;
	uint64_t first_block_table_block;
} __attribute__((packed));

struct fs_symlink {
	struct fs_element element;
	uint8_t path[];
} __attribute__((packed));

#define MOUNTPOINT_FLAGS_TYPE_TEMP         UINT8_C(0x01)
#define MOUNTPOINT_FLAGS_TYPE_FS_UUID      UINT8_C(0x02)
#define MOUNTPOINT_FLAGS_TYPE_FS_NAME      UINT8_C(0x03)
#define MOUNTPOINT_FLAGS_TYPE_PART_UUID    UINT8_C(0x04)
#define MOUNTPOINT_FLAGS_TYPE_PART_NAME    UINT8_C(0x05)
#define MOUNTPOINT_FLAGS_TYPE_MASK         UINT8_C(0x0F)

#define MOUNTPOINT_FLAGS_READ_ONLY         UINT8_C(0x10)
#define MOUNTPOINT_FLAGS_TMP_MOUNT_SUPPORT UINT8_C(0x20)
#define MOUNTPOINT_FLAGS_NO_DEF_ROOT       UINT8_C(0x40)

struct fs_mountpoint {
	struct fs_element element;
	uint8_t flags;
	uint8_t data[];
} __attribute__((packed));

struct fs_mountpoint_temp {
	struct fs_mountpoint mountpoint;
	uint64_t block_count;
	uint8_t block_size_shift;
} __attribute__((packed));

struct fs_mountpoint_uuid {
	struct fs_mountpoint mountpoint;
	uuid_t uuid;
} __attribute__((packed));

struct fs_mountpoint_name {
	struct fs_mountpoint mountpoint;
	// if the MOUNTPOINT_FLAGS_NO_DEF_ROOT flag is set name
	// is \0 terminated and after it is the root path
	char name[];
} __attribute__((packed));

#endif /* SRC_OROSFS_DEFS_H_ */
