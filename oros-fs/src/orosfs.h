// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs.h
 *
 *  Created on: Apr 20, 2024
 *      Author: pat
 */

#ifndef SRC_OROSFS_H_
#define SRC_OROSFS_H_

#include "orosfs-defs.h"
#include "export/orosfs.h"
#include "orosfs-block-table.h"
#include "export/orosfs-folder.h"
#include "export/orosfs-constants.h"

#define ELEMENT_HANDLE_FLAG_POTENTIAL_RW       UINT32_C(0x80000000)

#define rw_handle(h) ( \
	((h)->flags & ELEMENT_HANDLE_FLAG_POTENTIAL_RW) \
		? (h) \
		: (h)->handlesOrCounts.handles.rwHandle \
)

struct refCnt_modCnt {
	uint64_t modCount;
	uint64_t reference_count;
};

struct handles {
	struct element_handle *rwHandle;
};

union handle_ref_or_rw_stuff {
	struct refCnt_modCnt rcMc;
	struct handles handles;
};

struct element_handle {
	struct orosfs *fs;
	struct place place;
	uint32_t flags;
	union handle_ref_or_rw_stuff handlesOrCounts;
};

struct stream_handle {
	struct element_handle *rw_handle;
	uint64_t position;
	uint64_t modCount;
	struct place current_place;
};

struct dirstream_handle {
	const struct element_handle *rw_handle;
	uint64_t modCount;
	struct place current_place;
	uint32_t child_flags;
};

handle add_handle_use(handle use_freely);

handle add_handle(handle duplicate_and_add);

uint64_t handle_hash(const void* hash_me);

bool handle_equal(const void* a, const void* b);

uint64_t fs_hash(const void* hash_me);

bool fs_equal(const void* a, const void* b);

struct hs_mount_point_entry {
	mountpoint_handle mp;
	handle target;
	uint_fast32_t mount_count;
};

uint64_t mp_hash(const void* hash_me);

bool mp_equal(const void* a, const void* b);

bool ofs_descendant_impl(folder_handle folder, const char *path,
		size_t path_len, int mode, struct orosfs **const restrict fs,
		struct place *const restrict place, uint32_t *const restrict cur_flags,
		unsigned *const restrict link_count);

#ifdef OROSFS_HANDLES
#define OFS_EXT
#define OFS_INIT(...) = __VA_ARGS__
#else
#define OFS_EXT extern
#define OFS_INIT(...)
#endif

OFS_EXT struct hashset file_systems OFS_INIT({
		.equalizer = fs_equal,
		.hashmaker = fs_hash
});

OFS_EXT struct hashset mount_points OFS_INIT({
		.equalizer = mp_equal,
		.hashmaker = mp_hash
});

#undef OFS_EXT
#undef OFS_INIT

#endif /* SRC_OROSFS_H_ */
