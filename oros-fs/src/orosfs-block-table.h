// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs-block-table.h
 *
 *  Created on: Apr 19, 2024
 *      Author: pat
 */

#ifndef SRC_OROSFS_BLOCK_TABLE_H_
#define SRC_OROSFS_BLOCK_TABLE_H_

#include "orosfs-defs.h"

bool obt_free(void *block, uint32_t block_size, uint32_t location);

uint32_t obt_alloc(void *block, uint32_t block_size, uint32_t size);

bool obt_grow_shrink(void *block, uint32_t block_size, uint32_t location, uint32_t new_size);

uint32_t obt_get_size(void *block, uint32_t block_size, uint32_t location);

#endif /* SRC_OROSFS_BLOCK_TABLE_H_ */
