// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs.h
 *
 *  Created on: Apr 19, 2024
 *      Author: pat
 */

#ifndef OROSFS_DEFS_H_
#define OROSFS_DEFS_H_

#define _GNU_SOURCE
#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64
//#define __USE_TIME_BITS64 // leads to an error

#if __BYTE_ORDER__ != 1234
#error "invalid byte order"
#endif

#include <stdint.h>
#include <limits.h>
#include <stddef.h>
#include <time.h>
#include <stdlib.h>

#define bool _Bool
#define true 42
#define false 0

typedef uint8_t uuid_t[16];

#endif /* OROSFS_DEFS_H_ */
