/*
 * orosfs-constants.h
 *
 *  Created on: Apr 24, 2024
 *      Author: pat
 */

#ifndef OROSFS_CONSTANTS_H_
#define OROSFS_CONSTANTS_H_

#define ELEMENT_FLAG_FOLDER     UINT32_C(0x01)
#define ELEMENT_FLAG_FILE       UINT32_C(0x02)
#define ELEMENT_FLAG_SYMLINK    UINT32_C(0x04)
#define ELEMENT_FLAG_MOUNTPOINT UINT32_C(0x08)
#define ELEMENT_FLAG_TYPE_MASK  UINT32_C(0x000000FF)
#define ELEMENT_FLAG_READ_ONLY  UINT32_C(0x00000100)
#define ELEMENT_FLAG_EXECUTABLE UINT32_C(0x00000200)
#define ELEMENT_FLAG_EXEC_WRAP  UINT32_C(0x00000400)
#define ELEMENT_FLAG_MASK       UINT32_C(0x000007FF)

/*
 * this value specifies how many symbolic links and
 * mount points are allowed to occur during the resolution
 * of a path
 * mount points also count here because it is possible to
 * specify a path in the mount point which acts like an
 * initial symbolic link
 *
 * increase this value to allow more symbolic links to
 * occur in a path
 * decrease this value to improve the failure speed when
 * there are too many symbolic links
 */
#define OFS_MAX_LINK_COUNT 64

#endif /* OROSFS_CONSTANTS_H_ */
