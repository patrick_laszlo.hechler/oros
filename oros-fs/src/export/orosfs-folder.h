// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs-folder.h
 *
 *  Created on: Apr 20, 2024
 *      Author: pat
 */

#ifndef OROSFS_FOLDER_H_
#define OROSFS_FOLDER_H_

#include "orosfs.h"

/*
 * do not follow the final symbolic link (if exist)
 * ofs_descendant then returns the link instead
 * ofs_descendant_symlink has this mode always set
 * ofs_descendant_* will fail if the final element is a symbolic link
 */
#define OFS_DESC_MODE_NO_FOLLOW_SYMLINK 0x01
/*
 * do not allow mount-points to occur in the path except for the final element
 */
#define OFS_DESC_MODE_ONE_FILE_SYSTEM   0x02
/*
 * do not allow the final element to be a mount point
 * note that mount points can mount to any file system element,
 * so this can be useful even if you use ofs_descendant_file or similar
 * note that if this is set ofs_descendant_mountpoint will always fail
 */
#define OFS_DESC_MODE_NO_MOUNT_POINT    0x04
/*
 * do not follow the final mount point link (if exist)
 * ofs_descendant then returns the mount point instead
 * ofs_descendant_mountpoint has this mode always set
 * ofs_descendant_* will fail if the final element is a mount point link
 */
#define OFS_DESC_MODE_NO_OPEN_MOUNT_POINT    0x08
/*
 * do not allow mount-points to occur in the path and also do not allow
 * the final element to be a mount point
 */
#define OFS_DESC_MODE_ONE_FILE_SYSTEM2  \
	(OFS_DESC_MODE_ONE_FILE_SYSTEM | OFS_DESC_MODE_NO_MOUNT_POINT)

#define OFS_DESC_MODE_MASK  0x0F
#define OFS_DESC_MODE_BITS  4

/*
 * return a stream which can be used to iterate over the direct children of this folder
 */
dirstream ofs_children(folder_handle folder, int mode);

/*
 * returns the descendant element of the folder when following the path
 */
handle ofs_descendant(folder_handle folder, const char *path, int mode);

/*
 * returns the descendant folder of the folder when following the path
 * this operation fails if the result would be no folder
 */
folder_handle ofs_descendant_folder(folder_handle folder, const char *path,
		int mode);

/*
 * returns the descendant file of the folder when following the path
 * this operation fails if the result would not be a file
 */
file_handle ofs_descendant_file(folder_handle folder, const char *path,
		int mode);

/*
 * returns the descendant symbolic link of the folder when following the path
 * this operation fails if the result would not be a symbolic link
 */
symlink_handle ofs_descendant_symlink(folder_handle folder, const char *path,
		int mode);

/*
 * returns the descendant mount-point of the folder when following the path
 * this operation fails if the result would not be a mount-point
 * this operation fails if
 */
mountpoint_handle ofs_descendant_mountpoint(folder_handle folder,
		const char *path, int mode);

/*
 * create a folder at the sub-path of the folder
 */
folder_handle ofs_create_folder(folder_handle folder, const char *path);

/*
 * create a file at the sub-path of the folder
 */
file_handle ofs_create_file(folder_handle folder, const char *path);

/*
 * create a symbolic link at the sub-path of the folder with the target
 */
symlink_handle ofs_create_symlink(folder_handle folder, const char *path,
		const char *target);

/*
 * create a mount point at the sub-path of the folder
 * the mount point will mount a temporary file system
 * the temporary file system will have block_count blocks
 * and a block size of 1 << block_size_shift
 * if support_mounts has a non-zero value the file system
 * will support mount points
 * if stay_open has a non-zero value the mount point will
 * not be closed when the last handle inside it is closed
 */
mountpoint_handle ofs_create_mountpoint_tmp(folder_handle folder,
		const char *path, bool support_mounts, bool stay_open,
		uint64_t block_count, uint8_t block_size_shift);

/*
 * create a mount point at the sub-path of the folder
 * the mount point will mount the file system with the devie_name
 * if partition_name has a non-zero value the partition name
 * will be used instead of the file system name
 * if file_system is NULL the mount point will try during the
 * mount process to find out which file system should be used
 */
mountpoint_handle ofs_create_mountpoint_name(folder_handle folder,
		const char *path, const char *device_name, bool partition_name,
		const char *file_system);

/*
 * create a mount point at the sub-path of the folder
 * the mount point will mount the file system with the device_uuid
 * if partition_uuid has a non-zero value the partition UUID
 * will be used instead of the file system UUID
 * if file_system is NULL the mount point will try during the
 * mount process to find out which file system should be used
 */
mountpoint_handle ofs_create_mountpoint_uuid(folder_handle folder,
		const char *path, const uuid_t device_uuid, bool partition_uuid,
		const char *file_system);

/*
 * create a hard link to the target at the sub-path of the folder
 * this operation fails if the target and the path are on different
 * file systems
 * note that a folder hard link can not easily be deleted because the
 * folder needs to be empty before it can be deleted
 */
handle ofs_create_hardlink(folder_handle folder, const char *path,
		handle target);

/*
 * create a hard link to the target at the sub-path of the folder
 * this operation fails if the target and the path are on different
 * file systems
 * note that a folder hard link can not easily be deleted because the
 * folder needs to be empty before it can be deleted
 */
handle ofs_create_hardlink_path(folder_handle folder, const char *path,
		const char *target);

/*
 * move the descendant element at src_path to dst_path
 * if the final element of src_path is a folder and dst_path contains
 * the final element of src_path at any point the operation fails
 * note that this logic forbids some totally valid moves if hard-links
 * are involved
 */
handle ofs_move(folder_handle folder, const char *dst_path,
		const char *src_path);

/*
 * remove the descendant element at path from the file system
 * if the element is a folder and is not empty the operation fails
 */
handle ofs_remove(folder_handle folder, const char *path);

/*
 * follow the mount point
 */
handle ofs_follow_mount(mountpoint_handle symlink);

/*
 * read the path of the symbolic link
 * if buf_len is not large enough realloc will be called to ensure
 * that the symbolic links path fits in the buffer
 */
void ofs_read_symlink(symlink_handle symlink, char **buf, size_t *buf_len);

/*
 * get the length of the symbolic links path
 */
size_t ofs_read_symlink_len(symlink_handle symlink);

/*
 * return the next child element of the given directory stream
 * name_buf is filled with the name of the child
 * flags is set to the flags of the child element
 */
handle ofs_dirstr_next(dirstream stream, char **name_buf, size_t buf_size,
		uint32_t *flags);

#endif /* OROSFS_FOLDER_H_ */
