// SPDX-License-Identifier: AGPL-3.0-or-later

/*
 * orosfs.h
 *
 *  Created on: Apr 20, 2024
 *      Author: pat
 */

#ifndef OROSFS_H_
#define OROSFS_H_

#include "orosfs-defs.h"
#include "bm.h"

struct element_handle;
struct stream_handle;
struct dirstream_handle;

typedef struct element_handle *handle;
typedef struct stream_handle *stream;
typedef struct dirstream_handle *dirstream;

typedef handle folder_handle;
typedef handle file_handle;
typedef handle mountpoint_handle;
typedef handle symlink_handle;

folder_handle new_root(struct bm_block_manager *block_manager);

folder_handle format_new_root(struct bm_block_manager *block_manager,
		uint64_t block_count, uuid_t uuid, const char *name);

#endif /* OROSFS_H_ */
