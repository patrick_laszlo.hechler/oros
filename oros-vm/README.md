
<!-- SPDX-License-Identifier: GFDL-1.3-no-invariants-or-later -->

# OROS - VM - Spec
Only Root Operating System - Virtual Maschine - Specification

Virtual Machine for the reference implementation of the *OROS* System

    Copyright (C)  2024 Patrick Hechler.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

## Register
+ `IP` instruction pointer
+ `SP` stack pointer
+ `R0` .. `RF` general usage registers

### Status Register
The status register is not in the register list because the program can never use it directly.    
The status register can only be examinated and changed by special commands.    
These Commands use (or set) the following Status Flags:
+ `LESS`
+ `GREATER`
+ `EQUAL`
+ `NAN`
+ `ALL-BITS`
+ `SOME-BITS`
+ `NONE-BITS`

## Commands
### Command Parameter
most commands have parameters, those parameters can be:
+ *MODIFIABLE*
    + a general usage register (for example `R5`)
    + an address:
        + a register (for example `[RA]`)
        + a register with a constant 16-bit signed offset (for example `[RA + 0]`)
            + if the 8-bit flag is set the offset is also 8-bit sized
        + a register with a general usage register offset (for example `[SP + R1]`)
        + a register with a general usage register negative offset (for example `[SP - R1]`)
+ *CONSTANT*
    + a constant value
    + usually an address relative from the `IP`
+ *ANY*
    + same as *&lt;CONSTANT | MODIFIABLE | IP | SP&gt;*

#### One Parameter
A Parameter is encoded according to the following list.    
Note that if a Parameter ends in the middle of a byte (for example `[R0 + R1]`) the 4 unused bits should be zero padded, otherwise the behavior of the virtual mashine is undefined.    
If there is only one of the entries in the list whould be valid for a parameter the type is omitted.
+ constant value: `00`
    1. the next bytes contains the value
+ general usage register: `01`
    1. the 4 high bits of the parameter byte contain the used register
+ IP: `02`
+ SP: `03`
+ general usage register address: `04`
    1. the 4 high bits of the parameter byte contain the used register
+ IP address: `05`
+ SP address: `06`
+ general usage register address with 16/8-bit offset: `07`
    1. the 4 high bits of the parameter byte contain the used register
    2. the next 1/2-bytes contains the offset
+ IP address with 16/8-bit offset: `08`
    1. the next 1/2-bytes contains the offset
+ SP address with 16/8-bit offset: `09`
    1. the next 1/2-bytes contains the offset
+ general usage register address with a general usage register offset: `0A`
    1. the 4 high bits of the parameter byte contain the used register
    2. the next byte contains the used offset register
+ IP address with a general usage register offset: `0B`
    1. the 4 high bits of the parameter byte contain the used register
+ SP address with a general usage register offset: `0C`
    1. the 4 high bits of the parameter byte contain the used register
+ general usage register address with a negative general usage register offset: `0D`
    1. the 4 high bits of the parameter byte contain the used register
    2. the next byte contains the used offset register
+ IP address with a negative general usage register offset: `0E`
    1. the 4 high bits of the parameter byte contain the used offset register
+ SP address with a negative general usage register offset: `0F`
    1. the 4 high bits of the parameter byte contain the used offset register

#### Two Parameters
The first parameter is completly stored **after** the second parameter parameter.

### Command Flags
Some commands may have flags.
These flags modify the behavior of the commands.    
To flag a command use `FLAG-COMMAND` as `COMMAND` (with the `-`).    
It is possible to use multiple flags if the command supports it.    
If a flag is used on a command which does not support the flag command is treated like an illegal command.    
If any flags are valid, they are stored in the first byte half (marked with `~`).
+ `FP`
    + use signed floating point numbers
    + compatible with: `16`, `32`
    + value: `4` (`BIN-0100`)
+ `U`
    + use unsigned numbers
    + compatible with: `N`
    + value: `8` (`BIN-1000`)
+ `N`
    + use `N`-bit numbers instead of `64`-bit
    + `N` is only allowed to be `8`, `16` and `32`
    + `8`-bit:
        + compatible with: `U`
        + value: `1` (`BIN-0001`)
    + `16`-bit:
        + compatible with: `U`, `FP`
        + value: `2` (`BIN-0010`)
    + `32`-bit:
        + compatible with: `U`, `FP`
        + value: `3` (`BIN-0011`)
+ `64`
    + use `64`-bit numbers instead of `N`-bit
    + usually 64-bit is the default, so this flag is rarely supported
    + note that this flag is eliminated at translation time, because during runtime 64-bit is always the default
    + value: `0` (`BIN-0000`)
+ `BIG_N`
    + use `BIG_N`-bit numbers instead
    + it is not allowed to use these flags with registers as parameters
    + `BIG_N` is only allowed to be `8`, `16`, `32`, `128`, `256` and `512`
        + `8`, `16` and `32` are listed here, because all commands supporting the `BIG_N` flags implicitly also support the `N` flags
    + `128`-bit:
        + value: `5` (`BIN-0101`)
    + `256`-bit:
        + value: `6` (`BIN-0110`)
    + `512`-bit:
        + value: `7` (`BIN-0111`)

### Available Commands
#### Data Commands
+ `MOV` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + copy the data from the second parameter to the first parameter
    + flags: `BIG_N`
    + value: `100~`
    + behavior:
        1. `P0 <-- P1`
        2. `IP <-- IP + CMD_LEN`
+ `SWAP` *&lt;MODIFIABLE&gt;* *&lt;MODIFIABLE&gt;*
    + description:
        + swap the data of the parameters
    + flags: `BIG_N`
    + value: `200~`
    + behavior:
        1. `TMP0 <-- P0`
        2. `P0 <-- P1`
        3. `P1 <-- TMP0`
        4. `IP <-- IP + CMD_LEN`

#### Jump
+ `JMP` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `001~`
    + behavior:
        1. `IP <-- IP + P0`
+ `JMPGE` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `GREATER` or `EQUAL` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `101~`
    + behavior:
        1. `if GREATER or EQUAL`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPGT` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `GREATER` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `201~`
    + behavior:
        1. `if GREATER `
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPEQ` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `EQUAL` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `301~`
    + behavior:
        1. `if EQUAL`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPLT` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `LESS` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `401~`
    + behavior:
        1. `if LESS `
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPLE` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `LESS` or `EQUAL` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `501~`
    + behavior:
        1. `if LESS or EQUAL`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPNN` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `NAN` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `601~`
    + behavior:
        1. `if NAN`
            1. `IP <-- IP + CMD_LEN`
        2. `else`
            1. `IP <-- IP + P0`
+ `JMPAN` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `NAN` flag is not set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `701~`
    + behavior:
        1. `if NAN`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPAB` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `ALL-BITS` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `801~`
    + behavior:
        1. `if ALL-BITS`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPSB` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `SOME-BITS` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `901~`
    + behavior:
        1. `if SOME-BITS`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `JMPNB` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address if the `NONE-BITS` flag is set
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `A01~`
    + behavior:
        1. `if NONE-BITS`
            1. `IP <-- IP + P0`
        2. `else`
            1. `IP <-- IP + CMD_LEN`
+ `AJMP` *&lt;ANY&gt;*
    + description:
        + go to the absolute address
    + flags: `N`
    + value: `B01~`
    + behavior:
        1. `IP <-- P0`
+ `AOJMP` *&lt;ANY&gt;* *&lt;ANY&gt;*
    + description:
        + go to the absolute address at the given offset
    + flags: `N`
    + value: `C01~`
    + behavior:
        1. `IP <-- P0 + P1`

#### Stack
+ `CALL` *&lt;CONSTANT&gt;*
    + description:
        + go to the relative address and store the current address on the stack
        + if a label is used as address the assembler may decide to use an appropriate `N` flag
    + flags: `N`,  `64`
    + value: `002~`
    + behavior:
        1. `SP <-- SP - 8`
        2. `[SP] <-- IP + CMD_LEN`
        3. `IP <-- IP + P0`
+ `ACALL` *&lt;ANY&gt;*
    + description:
        + go to the absolute address and store the current address on the stack
    + flags: `N`
    + value: `102~`
    + behavior:
        1. `SP <-- SP - 8`
        2. `[SP] <-- IP + CMD_LEN`
        3. `IP <-- P0`
+ `AOCALL` *&lt;ANY&gt;* *&lt;ANY&gt;*
    + description:
        + go to the absolute address at the given offset and store the current address on the stack
    + flags: `N`
    + value: `202~`
    + behavior:
        1. `SP <-- SP - 8`
        2. `[SP] <-- IP + CMD_LEN`
        3. `IP <-- P0 + P1`
+ `RET`
    + description:
        + return execution to the invoker
    + flags: `N`
    + value: `302~`
    + behavior:
        1. `IP <-- [SP]`
        2. `SP <-- SP + 8`
+ `PUSH` *&lt;ANY&gt;*
    + description:
        + push the value to the stack
    + flags: `BIG_N`
    + value: `402~`
    + behavior:
        1. `SP <-- SP - NUMBER_BYTES`
        2. `[SP] <-- P0`
        3. `IP <-- IP + CMD_LEN`
+ `POP` *&lt;MODIFIABLE&gt;*
    + description:
        + pop the value from the stack to the param
    + flags: `BIG_N`
    + value: `502~`
    + behavior:
        1. `P0 <-- [SP]`
        2. `SP <-- SP + NUMBER_BYTES`
        3. `IP <-- IP + CMD_LEN`
+ `SAND` *&lt;ANY&gt;*
    + description:
        + set the stack pointer to the bitwise logical or value of itself and the parameter
        + the parameter is always treated as signed
        + this instruction can be used to align the stack
    + flags: `N`
    + value: `602~`
    + behavior:
        1. `SP <-- SP & <signed P0>` //treat P0 as signed to allow stack alignment with non-64 bit values
        2. `IP <-- IP + CMD_LEN`
+ `SSUB` *&lt;ANY&gt;*
    + description:
        + add the value of the parameter to the stack pointer
        + this instruction can be used to allocate space on the stack without initilizing it
    + flags: `N`, `U`
    + value: `702~`
    + behavior:
        1. `SP <-- SP - P0`
        2. `IP <-- IP + CMD_LEN`
+ `SMOV` *&lt;MODIFIABLE&gt;*
    + description:
        + set the stack pointer to the value of the parameter
    + flags:
    + value: `802~`
    + behavior:
        1. `SP <-- P0`
        2. `IP <-- IP + CMD_LEN`

#### System Interface
+ `CALLSYS`
    + description:
        + call the system to do something
    + flags: 
    + value: `003~`
    + behavior:
        1. `system()`
+ `ERR` *&lt;ANY&gt;*
    + description:
        + tell the system that you messed up
    + flags: 
    + value: `003~`
    + behavior:
        1. `validate_error_code(P0)`
        2. `IP <-- IP + CMD_LEN`
        3. `error(P0)`

#### Compare Values
+ `CMP` *&lt;ANY&gt;* *&lt;ANY&gt;*
    + description:
        + compare the two numerical values
    + flags: `FP`, `U`, `N`
    + value: `004~`
    + behavior:
        1. `if FP-FLAG`
            1. `if isNan(P1) || isNan(P2)`
                1. `NAN <-- 1`
                2. `LESS <-- 0`
                3. `EQUAL <-- 0`
                4. `GREATER <-- 0`
            2. `else`
                1. `if P1 > P2`
                    1. `NAN <-- 0`
                    2. `LESS <-- 0`
                    3. `EQUAL <-- 0`
                    4. `GREATER <-- 1`
                2. `else`
                    1. `if P1 < P2`
                        1. `NAN <-- 0`
                        2. `LESS <-- 1`
                        3. `EQUAL <-- 0`
                        4. `GREATER <-- 0`
                    2. `else`
                        1. `NAN <-- 0`
                        2. `LESS <-- 0`
                        3. `EQUAL <-- 1`
                        4. `GREATER <-- 0`
        2. `else`
            1. `if P1 > P2`
                1. `LESS <-- 0`
                2. `EQUAL <-- 0`
                3. `GREATER <-- 1`
            2. `else`
                1. `if P1 < P2`
                    1. `LESS <-- 1`
                    2. `EQUAL <-- 0`
                    3. `GREATER <-- 0`
                1. `else`
                    1. `LESS <-- 0`
                    2. `EQUAL <-- 1`
                    3. `GREATER <-- 0`
        3. `IP <-- IP + CMD_LEN`
+ `BCMP` *&lt;ANY&gt;* *&lt;ANY&gt;*
    + description:
        + compare the two logical values
    + flags: `N`
    + value: `104~`
    + behavior:
        1. `if (P1 & P2) == 0`
            1. `ALL-BITS <-- 0`
            2. `SOME-BITS <-- 0`
            3. `NONE-BITS <-- 1`
        1. `else`
            1. `if (P1 & P2) == P1`
                1. `ALL-BITS <-- 1`
                2. `SOME-BITS <-- 1`
                3. `NONE-BITS <-- 0`
            2. `else`
                1. `ALL-BITS <-- 0`
                2. `SOME-BITS <-- 1`
                3. `NONE-BITS <-- 0`
        3. `IP <-- IP + CMD_LEN`

#### Logic Mathematical Commands
+ `OR` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + set the first parameter to the bitwise logical or value of both parameters
    + flags: `N`
    + value: `005~`
    + behavior:
        1. `P0 <-- P0 | P1`
        2. `IP <-- IP + CMD_LEN`
+ `AND` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + set the first parameter to the bitwise logical or value of both parameters
    + flags: `N`
    + value: `105~`
    + behavior:
        1. `P0 <-- P0 & P1`
        2. `IP <-- IP + CMD_LEN`
+ `XOR` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + set the first parameter to the bitwise logical or value of both parameters
    + flags: `N`
    + value: `205~`
    + behavior:
        1. `P0 <-- P0 ^ P1`
        2. `IP <-- IP + CMD_LEN`

#### Arithmetic Mathematical Commands
+ `ADD` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + add the value of the second parameter to the first parameter
    + flags: `N`, `FP`, `U`
    + value: `006~`
    + behavior:
        1. `P0 <-- P0 + P1`
        2. `IP <-- IP + CMD_LEN`
+ `SUB` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + subtract the value of the second parameter from the first parameter
    + flags: `N`, `FP`, `U`
    + value: `106~`
    + behavior:
        1. `P0 <-- P0 - P1`
        2. `IP <-- IP + CMD_LEN`
+ `MUL` *&lt;MODIFIABLE&gt;* *&lt;ANY&gt;*
    + description:
        + multiply the value of the parameters and store the result in the first parameter
    + flags: `N`, `FP`, `U`
    + value: `206~`
    + behavior:
        1. `P0 <-- P0 * P1`
        2. `IP <-- IP + CMD_LEN`
+ `DIV` *&lt;MODIFIABLE&gt;* *&lt;MODIFIABLE&gt;*
    + description:
        + divide the first parameter with the second parameter, store the result of the division in the first parameter and the modulo result in the second parameter
    + flags: `N`, `FP`, `U`
    + value: `306~`
    + behavior:
        1. `TMP0 <-- P0`
        2. `P0 <-- TMP0 / P1`
        3. `P1 <-- TMP0 % P1`
        4. `IP <-- IP + CMD_LEN`

#### Convert Numbers
+ `NTF` *&lt;MODIFIABLE&gt;*
    + description:
        + convert the number to a floating point number
    + flags: `16`, `32`, `U`
    + value: `007~`
    + behavior:
        1. `P1 <-- (floating-point) P1`
        2. `IP <-- IP + CMD_LEN`
+ `FTN` *&lt;MODIFIABLE&gt;*
    + description:
        + convert the floating point number to a number
    + flags: `16`, `32`, `U`
    + value: `107~`
    + behavior:
        1. `P1 <-- (number) P1`
        2. `IP <-- IP + CMD_LEN`
+ `SRKFP` *&lt;MODIFIABLE&gt;*
    + description:
        + convert the floating point number to a floating point number of a smaller size
    + flags: `32`
    + value: `207~`
    + behavior:
        1. `if 32-FLAG`
            1. `<floating-point-16 P1> <-- (floating-point-16) P1`
        2. `else`
            1. `<floating-point-32 P1> <-- (floating-point-32) P1`
        3. `IP <-- IP + CMD_LEN`
+ `GRWFP` *&lt;MODIFIABLE&gt;*
    + description:
        + convert the floating point number to a floating point number of a larger size
    + flags: `32`
    + value: `307~`
    + behavior:
        1. `if 32-FLAG`
            1. `P1 <-- (floating-point-32) <floating-point-16 P1>`
        2. `else`
            1. `P1 <-- (floating-point-64) <floating-point-32 P1>`
        3. `IP <-- IP + CMD_LEN`
+ `GRWN` *&lt;MODIFIABLE&gt;*
    + description:
        + convert the number to a number of a larger size
    + flags: `16`, `32`
    + value: `407~`
    + behavior:
        1. `if 16-FLAG`
            1. `P1 <-- (number-16) <number-8 P1>`
        2. `else`
            1. `if 32-FLAG`
                1. `P1 <-- (number-32) <number-16 P1>`
            2. `else`
                1. `P1 <-- (number-64) <number-32 P1>`
        3. `IP <-- IP + CMD_LEN`

# GNU Free Documentation License

Version 1.3, 3 November 2008

Copyright (C) 2000, 2001, 2002, 2007, 2008 Free Software Foundation,
Inc. <https://fsf.org/>

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.

## 0. PREAMBLE

The purpose of this License is to make a manual, textbook, or other
functional and useful document "free" in the sense of freedom: to
assure everyone the effective freedom to copy and redistribute it,
with or without modifying it, either commercially or noncommercially.
Secondarily, this License preserves for the author and publisher a way
to get credit for their work, while not being considered responsible
for modifications made by others.

This License is a kind of "copyleft", which means that derivative
works of the document must themselves be free in the same sense. It
complements the GNU General Public License, which is a copyleft
license designed for free software.

We have designed this License in order to use it for manuals for free
software, because free software needs free documentation: a free
program should come with manuals providing the same freedoms that the
software does. But this License is not limited to software manuals; it
can be used for any textual work, regardless of subject matter or
whether it is published as a printed book. We recommend this License
principally for works whose purpose is instruction or reference.

## 1. APPLICABILITY AND DEFINITIONS

This License applies to any manual or other work, in any medium, that
contains a notice placed by the copyright holder saying it can be
distributed under the terms of this License. Such a notice grants a
world-wide, royalty-free license, unlimited in duration, to use that
work under the conditions stated herein. The "Document", below, refers
to any such manual or work. Any member of the public is a licensee,
and is addressed as "you". You accept the license if you copy, modify
or distribute the work in a way requiring permission under copyright
law.

A "Modified Version" of the Document means any work containing the
Document or a portion of it, either copied verbatim, or with
modifications and/or translated into another language.

A "Secondary Section" is a named appendix or a front-matter section of
the Document that deals exclusively with the relationship of the
publishers or authors of the Document to the Document's overall
subject (or to related matters) and contains nothing that could fall
directly within that overall subject. (Thus, if the Document is in
part a textbook of mathematics, a Secondary Section may not explain
any mathematics.) The relationship could be a matter of historical
connection with the subject or with related matters, or of legal,
commercial, philosophical, ethical or political position regarding
them.

The "Invariant Sections" are certain Secondary Sections whose titles
are designated, as being those of Invariant Sections, in the notice
that says that the Document is released under this License. If a
section does not fit the above definition of Secondary then it is not
allowed to be designated as Invariant. The Document may contain zero
Invariant Sections. If the Document does not identify any Invariant
Sections then there are none.

The "Cover Texts" are certain short passages of text that are listed,
as Front-Cover Texts or Back-Cover Texts, in the notice that says that
the Document is released under this License. A Front-Cover Text may be
at most 5 words, and a Back-Cover Text may be at most 25 words.

A "Transparent" copy of the Document means a machine-readable copy,
represented in a format whose specification is available to the
general public, that is suitable for revising the document
straightforwardly with generic text editors or (for images composed of
pixels) generic paint programs or (for drawings) some widely available
drawing editor, and that is suitable for input to text formatters or
for automatic translation to a variety of formats suitable for input
to text formatters. A copy made in an otherwise Transparent file
format whose markup, or absence of markup, has been arranged to thwart
or discourage subsequent modification by readers is not Transparent.
An image format is not Transparent if used for any substantial amount
of text. A copy that is not "Transparent" is called "Opaque".

Examples of suitable formats for Transparent copies include plain
ASCII without markup, Texinfo input format, LaTeX input format, SGML
or XML using a publicly available DTD, and standard-conforming simple
HTML, PostScript or PDF designed for human modification. Examples of
transparent image formats include PNG, XCF and JPG. Opaque formats
include proprietary formats that can be read and edited only by
proprietary word processors, SGML or XML for which the DTD and/or
processing tools are not generally available, and the
machine-generated HTML, PostScript or PDF produced by some word
processors for output purposes only.

The "Title Page" means, for a printed book, the title page itself,
plus such following pages as are needed to hold, legibly, the material
this License requires to appear in the title page. For works in
formats which do not have any title page as such, "Title Page" means
the text near the most prominent appearance of the work's title,
preceding the beginning of the body of the text.

The "publisher" means any person or entity that distributes copies of
the Document to the public.

A section "Entitled XYZ" means a named subunit of the Document whose
title either is precisely XYZ or contains XYZ in parentheses following
text that translates XYZ in another language. (Here XYZ stands for a
specific section name mentioned below, such as "Acknowledgements",
"Dedications", "Endorsements", or "History".) To "Preserve the Title"
of such a section when you modify the Document means that it remains a
section "Entitled XYZ" according to this definition.

The Document may include Warranty Disclaimers next to the notice which
states that this License applies to the Document. These Warranty
Disclaimers are considered to be included by reference in this
License, but only as regards disclaiming warranties: any other
implication that these Warranty Disclaimers may have is void and has
no effect on the meaning of this License.

## 2. VERBATIM COPYING

You may copy and distribute the Document in any medium, either
commercially or noncommercially, provided that this License, the
copyright notices, and the license notice saying this License applies
to the Document are reproduced in all copies, and that you add no
other conditions whatsoever to those of this License. You may not use
technical measures to obstruct or control the reading or further
copying of the copies you make or distribute. However, you may accept
compensation in exchange for copies. If you distribute a large enough
number of copies you must also follow the conditions in section 3.

You may also lend copies, under the same conditions stated above, and
you may publicly display copies.

## 3. COPYING IN QUANTITY

If you publish printed copies (or copies in media that commonly have
printed covers) of the Document, numbering more than 100, and the
Document's license notice requires Cover Texts, you must enclose the
copies in covers that carry, clearly and legibly, all these Cover
Texts: Front-Cover Texts on the front cover, and Back-Cover Texts on
the back cover. Both covers must also clearly and legibly identify you
as the publisher of these copies. The front cover must present the
full title with all words of the title equally prominent and visible.
You may add other material on the covers in addition. Copying with
changes limited to the covers, as long as they preserve the title of
the Document and satisfy these conditions, can be treated as verbatim
copying in other respects.

If the required texts for either cover are too voluminous to fit
legibly, you should put the first ones listed (as many as fit
reasonably) on the actual cover, and continue the rest onto adjacent
pages.

If you publish or distribute Opaque copies of the Document numbering
more than 100, you must either include a machine-readable Transparent
copy along with each Opaque copy, or state in or with each Opaque copy
a computer-network location from which the general network-using
public has access to download using public-standard network protocols
a complete Transparent copy of the Document, free of added material.
If you use the latter option, you must take reasonably prudent steps,
when you begin distribution of Opaque copies in quantity, to ensure
that this Transparent copy will remain thus accessible at the stated
location until at least one year after the last time you distribute an
Opaque copy (directly or through your agents or retailers) of that
edition to the public.

It is requested, but not required, that you contact the authors of the
Document well before redistributing any large number of copies, to
give them a chance to provide you with an updated version of the
Document.

## 4. MODIFICATIONS

You may copy and distribute a Modified Version of the Document under
the conditions of sections 2 and 3 above, provided that you release
the Modified Version under precisely this License, with the Modified
Version filling the role of the Document, thus licensing distribution
and modification of the Modified Version to whoever possesses a copy
of it. In addition, you must do these things in the Modified Version:

-   A. Use in the Title Page (and on the covers, if any) a title
    distinct from that of the Document, and from those of previous
    versions (which should, if there were any, be listed in the
    History section of the Document). You may use the same title as a
    previous version if the original publisher of that version
    gives permission.
-   B. List on the Title Page, as authors, one or more persons or
    entities responsible for authorship of the modifications in the
    Modified Version, together with at least five of the principal
    authors of the Document (all of its principal authors, if it has
    fewer than five), unless they release you from this requirement.
-   C. State on the Title page the name of the publisher of the
    Modified Version, as the publisher.
-   D. Preserve all the copyright notices of the Document.
-   E. Add an appropriate copyright notice for your modifications
    adjacent to the other copyright notices.
-   F. Include, immediately after the copyright notices, a license
    notice giving the public permission to use the Modified Version
    under the terms of this License, in the form shown in the
    Addendum below.
-   G. Preserve in that license notice the full lists of Invariant
    Sections and required Cover Texts given in the Document's
    license notice.
-   H. Include an unaltered copy of this License.
-   I. Preserve the section Entitled "History", Preserve its Title,
    and add to it an item stating at least the title, year, new
    authors, and publisher of the Modified Version as given on the
    Title Page. If there is no section Entitled "History" in the
    Document, create one stating the title, year, authors, and
    publisher of the Document as given on its Title Page, then add an
    item describing the Modified Version as stated in the
    previous sentence.
-   J. Preserve the network location, if any, given in the Document
    for public access to a Transparent copy of the Document, and
    likewise the network locations given in the Document for previous
    versions it was based on. These may be placed in the "History"
    section. You may omit a network location for a work that was
    published at least four years before the Document itself, or if
    the original publisher of the version it refers to
    gives permission.
-   K. For any section Entitled "Acknowledgements" or "Dedications",
    Preserve the Title of the section, and preserve in the section all
    the substance and tone of each of the contributor acknowledgements
    and/or dedications given therein.
-   L. Preserve all the Invariant Sections of the Document, unaltered
    in their text and in their titles. Section numbers or the
    equivalent are not considered part of the section titles.
-   M. Delete any section Entitled "Endorsements". Such a section may
    not be included in the Modified Version.
-   N. Do not retitle any existing section to be Entitled
    "Endorsements" or to conflict in title with any Invariant Section.
-   O. Preserve any Warranty Disclaimers.

If the Modified Version includes new front-matter sections or
appendices that qualify as Secondary Sections and contain no material
copied from the Document, you may at your option designate some or all
of these sections as invariant. To do this, add their titles to the
list of Invariant Sections in the Modified Version's license notice.
These titles must be distinct from any other section titles.

You may add a section Entitled "Endorsements", provided it contains
nothing but endorsements of your Modified Version by various
parties—for example, statements of peer review or that the text has
been approved by an organization as the authoritative definition of a
standard.

You may add a passage of up to five words as a Front-Cover Text, and a
passage of up to 25 words as a Back-Cover Text, to the end of the list
of Cover Texts in the Modified Version. Only one passage of
Front-Cover Text and one of Back-Cover Text may be added by (or
through arrangements made by) any one entity. If the Document already
includes a cover text for the same cover, previously added by you or
by arrangement made by the same entity you are acting on behalf of,
you may not add another; but you may replace the old one, on explicit
permission from the previous publisher that added the old one.

The author(s) and publisher(s) of the Document do not by this License
give permission to use their names for publicity for or to assert or
imply endorsement of any Modified Version.

## 5. COMBINING DOCUMENTS

You may combine the Document with other documents released under this
License, under the terms defined in section 4 above for modified
versions, provided that you include in the combination all of the
Invariant Sections of all of the original documents, unmodified, and
list them all as Invariant Sections of your combined work in its
license notice, and that you preserve all their Warranty Disclaimers.

The combined work need only contain one copy of this License, and
multiple identical Invariant Sections may be replaced with a single
copy. If there are multiple Invariant Sections with the same name but
different contents, make the title of each such section unique by
adding at the end of it, in parentheses, the name of the original
author or publisher of that section if known, or else a unique number.
Make the same adjustment to the section titles in the list of
Invariant Sections in the license notice of the combined work.

In the combination, you must combine any sections Entitled "History"
in the various original documents, forming one section Entitled
"History"; likewise combine any sections Entitled "Acknowledgements",
and any sections Entitled "Dedications". You must delete all sections
Entitled "Endorsements".

## 6. COLLECTIONS OF DOCUMENTS

You may make a collection consisting of the Document and other
documents released under this License, and replace the individual
copies of this License in the various documents with a single copy
that is included in the collection, provided that you follow the rules
of this License for verbatim copying of each of the documents in all
other respects.

You may extract a single document from such a collection, and
distribute it individually under this License, provided you insert a
copy of this License into the extracted document, and follow this
License in all other respects regarding verbatim copying of that
document.

## 7. AGGREGATION WITH INDEPENDENT WORKS

A compilation of the Document or its derivatives with other separate
and independent documents or works, in or on a volume of a storage or
distribution medium, is called an "aggregate" if the copyright
resulting from the compilation is not used to limit the legal rights
of the compilation's users beyond what the individual works permit.
When the Document is included in an aggregate, this License does not
apply to the other works in the aggregate which are not themselves
derivative works of the Document.

If the Cover Text requirement of section 3 is applicable to these
copies of the Document, then if the Document is less than one half of
the entire aggregate, the Document's Cover Texts may be placed on
covers that bracket the Document within the aggregate, or the
electronic equivalent of covers if the Document is in electronic form.
Otherwise they must appear on printed covers that bracket the whole
aggregate.

## 8. TRANSLATION

Translation is considered a kind of modification, so you may
distribute translations of the Document under the terms of section 4.
Replacing Invariant Sections with translations requires special
permission from their copyright holders, but you may include
translations of some or all Invariant Sections in addition to the
original versions of these Invariant Sections. You may include a
translation of this License, and all the license notices in the
Document, and any Warranty Disclaimers, provided that you also include
the original English version of this License and the original versions
of those notices and disclaimers. In case of a disagreement between
the translation and the original version of this License or a notice
or disclaimer, the original version will prevail.

If a section in the Document is Entitled "Acknowledgements",
"Dedications", or "History", the requirement (section 4) to Preserve
its Title (section 1) will typically require changing the actual
title.

## 9. TERMINATION

You may not copy, modify, sublicense, or distribute the Document
except as expressly provided under this License. Any attempt otherwise
to copy, modify, sublicense, or distribute it is void, and will
automatically terminate your rights under this License.

However, if you cease all violation of this License, then your license
from a particular copyright holder is reinstated (a) provisionally,
unless and until the copyright holder explicitly and finally
terminates your license, and (b) permanently, if the copyright holder
fails to notify you of the violation by some reasonable means prior to
60 days after the cessation.

Moreover, your license from a particular copyright holder is
reinstated permanently if the copyright holder notifies you of the
violation by some reasonable means, this is the first time you have
received notice of violation of this License (for any work) from that
copyright holder, and you cure the violation prior to 30 days after
your receipt of the notice.

Termination of your rights under this section does not terminate the
licenses of parties who have received copies or rights from you under
this License. If your rights have been terminated and not permanently
reinstated, receipt of a copy of some or all of the same material does
not give you any rights to use it.

## 10. FUTURE REVISIONS OF THIS LICENSE

The Free Software Foundation may publish new, revised versions of the
GNU Free Documentation License from time to time. Such new versions
will be similar in spirit to the present version, but may differ in
detail to address new problems or concerns. See
<https://www.gnu.org/licenses/>.

Each version of the License is given a distinguishing version number.
If the Document specifies that a particular numbered version of this
License "or any later version" applies to it, you have the option of
following the terms and conditions either of that specified version or
of any later version that has been published (not as a draft) by the
Free Software Foundation. If the Document does not specify a version
number of this License, you may choose any version ever published (not
as a draft) by the Free Software Foundation. If the Document specifies
that a proxy can decide which future versions of this License can be
used, that proxy's public statement of acceptance of a version
permanently authorizes you to choose that version for the Document.

## 11. RELICENSING

"Massive Multiauthor Collaboration Site" (or "MMC Site") means any
World Wide Web server that publishes copyrightable works and also
provides prominent facilities for anybody to edit those works. A
public wiki that anybody can edit is an example of such a server. A
"Massive Multiauthor Collaboration" (or "MMC") contained in the site
means any set of copyrightable works thus published on the MMC site.

"CC-BY-SA" means the Creative Commons Attribution-Share Alike 3.0
license published by Creative Commons Corporation, a not-for-profit
corporation with a principal place of business in San Francisco,
California, as well as future copyleft versions of that license
published by that same organization.

"Incorporate" means to publish or republish a Document, in whole or in
part, as part of another Document.

An MMC is "eligible for relicensing" if it is licensed under this
License, and if all works that were first published under this License
somewhere other than this MMC, and subsequently incorporated in whole
or in part into the MMC, (1) had no cover texts or invariant sections,
and (2) were thus incorporated prior to November 1, 2008.

The operator of an MMC Site may republish an MMC contained in the site
under CC-BY-SA on the same site at any time before August 1, 2009,
provided the MMC is eligible for relicensing.

## ADDENDUM: How to use this License for your documents

To use this License in a document you have written, include a copy of
the License in the document and put the following copyright and
license notices just after the title page:

        Copyright (C)  YEAR  YOUR NAME.
        Permission is granted to copy, distribute and/or modify this document
        under the terms of the GNU Free Documentation License, Version 1.3
        or any later version published by the Free Software Foundation;
        with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
        A copy of the license is included in the section entitled "GNU
        Free Documentation License".

If you have Invariant Sections, Front-Cover Texts and Back-Cover
Texts, replace the "with … Texts." line with this:

        with the Invariant Sections being LIST THEIR TITLES, with the
        Front-Cover Texts being LIST, and with the Back-Cover Texts being LIST.

If you have Invariant Sections without Cover Texts, or some other
combination of the three, merge those two alternatives to suit the
situation.

If your document contains nontrivial examples of program code, we
recommend releasing these examples in parallel under your choice of
free software license, such as the GNU General Public License, to
permit their use in free software.
