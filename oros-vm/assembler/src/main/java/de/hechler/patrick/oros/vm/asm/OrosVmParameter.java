// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.util.List;
import java.util.Map;

public interface OrosVmParameter {
	
	public static final int TYPE_CONST       = 1; // 7
	public static final int TYPE_REG         = 2; // R7
	public static final int TYPE_REG_ADDR    = 3; // [R7]
	public static final int TYPE_REG_16OFF   = 4; // [R7 + 7]
	public static final int TYPE_REG_GUROFF  = 5; // [R7 + R0]
	public static final int TYPE_REG_NGUROFF = 6; // [R7 + R0]
	
	OrosVmParam toParam(List<OrosVmCommand> cmds, int index, long commandOffset, Map<String, Long> labels);
	
}
