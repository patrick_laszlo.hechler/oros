// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.util.List;
import java.util.Map;
import java.util.Set;

public record OrosVmParamLabel(Set<Flag> flags, String name) implements OrosVmParameter {
	
	public static OrosVmParamLabel createLabel(Set<Flag> flags, String name) {
		return new OrosVmParamLabel(flags, name);
	}
	
	@Override
	@SuppressWarnings("unused")
	public OrosVmParam toParam(List<OrosVmCommand> cmds, int index, long commandOffset, Map<String, Long> labels) {
		Long absoluteTarget = labels.get(name);
		if (absoluteTarget == null) {
			throw new IllegalStateException(
					"label '" + name + "' used but not defined! (labels: " + labels.keySet() + ')');
		}
		long value = commandOffset - absoluteTarget.longValue();
		int size;
		if (flags.contains(Flag.FLAG_8)) {
			size = 1;
		} else if (flags.contains(Flag.FLAG_16)) {
			size = 2;
		} else if (flags.contains(Flag.FLAG_32)) {
			size = 4;
		} else if (flags.contains(Flag.FLAG_64)) {
			size = 8;
		} else if (value == (byte) value) {
			size = 1;
		} else if (value == (short) value) {
			size = 2;
		} else if (value == (int) value) {
			size = 4;
		} else {
			size = 8;
		}
		byte[] data;
		if (size == 1) {
			if (value != (byte) value) throw new IllegalStateException("8-bit overflow");
			data = new byte[] { (byte) value };
		} else if (size == 2) {
			if (value != (short) value) throw new IllegalStateException("16-bit overflow");
			data = new byte[] { (byte) value, (byte) ( value >>> 8 ) };
		} else if (size == 4) {
			if (value != (int) value) throw new IllegalStateException("32-bit overflow");
			data = new byte[] { //
					(byte) value, (byte) ( value >>> 8 ), //
					(byte) ( value >>> 16 ), (byte) ( value >>> 24 )//
			};
		} else if (size == 8) {
			data = new byte[] { //
					(byte) value, (byte) ( value >>> 8 ), //
					(byte) ( value >>> 16 ), (byte) ( value >>> 24 ), //
					(byte) ( value >>> 32 ), (byte) ( value >>> 40 ), //
					(byte) ( value >>> 48 ), (byte) ( value >>> 56 )//
			};
		} else {
			throw new AssertionError("unknown size: " + size);
		}
		return OrosVmParam.createConstant(flags, data);
	}
	
}
