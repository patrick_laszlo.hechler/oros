// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.io.BufferedOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class HexOutputStream extends FilterOutputStream {
	
	public HexOutputStream(OutputStream out) {
		super(out instanceof BufferedOutputStream ? out : new BufferedOutputStream(out));
	}
	
	@Override
	public void write(int b) throws IOException {
		int h = b >>> 4;
		if (h < 10) {
			out.write('0' + h);
		} else {
			out.write('A' - 10 + h);
		}
		h = b & 0xF;
		if (h < 10) {
			out.write('0' + h);
		} else {
			out.write('A' - 10 + h);
		}
		out.write(' ');
	}
	
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		if (len == 0) return;
		if (len < 0 || off < 0 || b.length - off < len) {
			throw new IllegalArgumentException("b.len=" + b.length + ", off=" + off + ", len=" + len);
		}
		if (len << 1 < 0 || len * 3 < 0) {
			write(b, off, len / 3);
			write(b, off + ( len / 3 ), len / 3);
			write(b, off + ( ( len / 3 ) << 1 ), len - ( ( len / 3 ) << 1 ));
			return;
		}
		byte[] hex = new byte[len * 3];
		for (int i = 0, hi = 0; i < len; i++, hi += 3) {
			int bt = b[off + i];
			int h = bt >>> 4;
			if (h < 10) {
				hex[hi] = (byte) ( '0' + h );
			} else {
				hex[hi] = (byte) ( 'A' - 10 + h );
			}
			h = bt & 0xF;
			if (h < 10) {
				hex[hi + 1] = (byte) ( '0' + h );
			} else {
				hex[hi + 1] = (byte) ( 'A' - 10 + h );
			}
			hex[hi + 2] = ' ';
		}
		out.write(hex);
	}
	
}
