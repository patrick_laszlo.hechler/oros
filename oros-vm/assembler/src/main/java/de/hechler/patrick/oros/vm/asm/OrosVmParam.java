// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.emory.mathcs.backport.java.util.Arrays;

public class OrosVmParam implements OrosVmParameter {
	
	public static final int IP = 0x10;
	public static final int SP = 0x11;
	
	private final Set<Flag> flags;
	private final int       type;
	private final long      value;
	private final byte[]    data;
	
	private OrosVmParam(Set<Flag> flags, int type, long value) {
		this.flags = flags;
		this.type = type;
		this.value = value;
		this.data = null;
	}
	
	private OrosVmParam(Set<Flag> flags, int type, byte[] value) {
		this.flags = flags;
		this.type = type;
		this.value = 0;
		this.data = value;
	}
	
	public static OrosVmParam createConstant(Set<Flag> flags, byte[] value) {
		return new OrosVmParam(flags, TYPE_CONST, value);
	}
	
	public static OrosVmParam createConstant(Set<Flag> flags, long value) {
		return new OrosVmParam(flags, TYPE_CONST, value);
	}
	
	public static OrosVmParam createRegister(Set<Flag> flags, int register) {
		checkReg(register);
		return new OrosVmParam(flags, TYPE_REG, register);
	}
	
	public static OrosVmParam createRegisterAddress(Set<Flag> flags, int register) {
		checkReg(register);
		return new OrosVmParam(flags, TYPE_REG_ADDR, register);
	}
	
	public static OrosVmParam createRegisterConstOffset(Set<Flag> flags, int register, int offset) {
		checkReg(register);
		if (( offset & 0xFFFF ) != offset) {
			throw new IllegalArgumentException("invalid 16-bit offset: R" + Integer.toHexString(offset).toUpperCase());
		}
		return new OrosVmParam(flags, TYPE_REG_16OFF, ( register << 16 ) | offset);
	}
	
	public static OrosVmParam createRegisterRegOffset(Set<Flag> flags, int register, int offset) {
		checkReg(register);
		checkGUR(offset);
		return new OrosVmParam(flags, TYPE_REG_GUROFF, ( register << 16 ) | offset);
	}
	
	public static OrosVmParam createRegisterNegRegOffset(Set<Flag> flags, int register, int offset) {
		checkReg(register);
		checkGUR(offset);
		return new OrosVmParam(flags, TYPE_REG_NGUROFF, ( register << 16 ) | offset);
	}
	
	private static void checkReg(int reg) {
		if (reg == IP || reg == SP) return;
		checkGUR(reg);
	}
	
	private static void checkGUR(int gur) {
		if (( gur & 0xF ) != gur) {
			throw new IllegalArgumentException(
					"invalid general usage register: R" + Integer.toHexString(gur).toUpperCase());
		}
	}
	
	public int type() {
		switch (type) {
		case TYPE_CONST:
			return 0x0;
		case TYPE_REG:
			if (IP == value) return 0x2;
			if (SP == value) return 0x3;
			return 0x1;
		case TYPE_REG_ADDR:
			if (IP == value) return 0x5;
			if (SP == value) return 0x6;
			return 0x4;
		case TYPE_REG_16OFF:
			if (IP == value) return 0x8;
			if (SP == value) return 0x9;
			return 0x7;
		case TYPE_REG_GUROFF:
			if (IP == value) return 0xB;
			if (SP == value) return 0xC;
			return 0xA;
		case TYPE_REG_NGUROFF:
			if (IP == value) return 0xE;
			if (SP == value) return 0xF;
			return 0xD;
		default:
			throw new AssertionError("invalid param: [type=" + type + ", value=" + value + " : 0x"
					+ Long.toHexString(value).toUpperCase() + ']');
		}
	}
	
	public int dataLength() {
		switch (type) {
		case TYPE_CONST:
			if (flags.contains(Flag.FLAG_8)) return 1;
			if (flags.contains(Flag.FLAG_16)) return 2;
			if (flags.contains(Flag.FLAG_32)) return 4;
			if (flags.contains(Flag.FLAG_128)) return 16;
			if (flags.contains(Flag.FLAG_256)) return 32;
			if (flags.contains(Flag.FLAG_512)) return 64;
			return 8;
		case TYPE_REG:
		case TYPE_REG_ADDR:
			if (IP == value) return 0;
			if (SP == value) return 0;
			return 1;
		case TYPE_REG_16OFF:
			int add = 2;
			if (flags.contains(Flag.FLAG_8)) add = 1;
			if (IP == value) return add;
			if (SP == value) return add;
			return 1 + add;
		case TYPE_REG_GUROFF:
		case TYPE_REG_NGUROFF:
			if (IP == value) return 1;
			if (SP == value) return 1;
			return 2;
		default:
			throw new AssertionError("invalid param: [type=" + type + ", value=" + value + " : 0x"
					+ Long.toHexString(value).toUpperCase() + ']');
		}
	}
	
	public byte[] data() {
		byte[] result = new byte[dataLength()];
		data(result, 0);
		return result;
	}
	
	public void data(byte[] buf, int off) {
		assert buf.length - off >= dataLength();
		switch (type) {
		case TYPE_CONST:
			if (data != null) {
				assert data.length == dataLength();
				System.arraycopy(data, 0, buf, off, data.length);
			} else {
				
			}
			break;
		case TYPE_REG:
		case TYPE_REG_ADDR:
			if (IP == value || SP == value) break;
			buf[off] = (byte) value;
			break;
		case TYPE_REG_16OFF:
			int addOff = 1;
			if (IP == value || SP == value) addOff = 0;
			else buf[off] = (byte) ( value >> 16 );
			if (flags.contains(Flag.FLAG_8)) buf[off + addOff] = (byte) value;
			else {
				buf[off + addOff] = (byte) value;
				buf[off + addOff + 1] = (byte) ( value >>> 8 );
			}
			break;
		case TYPE_REG_GUROFF:
		case TYPE_REG_NGUROFF:
			if (IP != value && SP != value) {
				buf[off] = (byte) value;
			} else {
				buf[off] = (byte) ( value >> 16 );
				buf[off] = (byte) value;
			}
			break;
		default:
			throw new AssertionError("invalid param: [type=" + type + ", value=" + value + " : 0x"
					+ Long.toHexString(value).toUpperCase() + ']');
		}
	}
	
	@Override
	public String toString() {
		switch (type) {
		case TYPE_CONST:
			return data == null ? Long.toHexString(value) : Arrays.toString(data);
		case TYPE_REG:
			return regToString((int) value);
		case TYPE_REG_ADDR:
			return '[' + regToString((int) value) + ']';
		case TYPE_REG_16OFF:
			int off = (short) value;
			if (off >= 0) {
				return '[' + regToString(( (int) value >>> 16 ) & 0xFF) + " + " + off + ']';
			} else {
				return '[' + regToString(( (int) value >>> 16 ) & 0xFF) + " - " + -off + ']';
			}
		case TYPE_REG_GUROFF:
			return '[' + regToString(( (int) value >>> 16 ) & 0xFF) + " + " + regToString((byte) value) + ']';
		case TYPE_REG_NGUROFF:
			return '[' + regToString(( (int) value >>> 16 ) & 0xFF) + " - " + regToString((byte) value) + ']';
		default:
			return "INVALID PARAM: [type=" + type + ", value=" + value + " : 0x" + Long.toHexString(value).toUpperCase()
					+ ']';
		}
	}
	
	private static String regToString(int reg) {
		if (reg == IP) return "IP";
		if (reg == SP) return "SP";
		return "R" + Integer.toHexString(reg).toUpperCase();
	}
	
	@Override
	@SuppressWarnings("unused")
	public OrosVmParam toParam(List<OrosVmCommand> cmds, int index, long commandOffset, Map<String, Long> labels) {
		return this;
	}
	
}
