// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class OVAMain {
	
	public static void main(String[] args) throws IOException {
		Path inp = null;
		Path outp = null;
		int force = 0;
		int hex = 0;
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "--help" -> argHelp();
			case "--license" -> argLicense();
			case "--in" -> inp = argFile(args, ++i, "input", inp);
			case "--out" -> outp = argFile(args, ++i, "output", outp);
			case "--force" -> force = argFlag(force, "force", 1);
			case "--no-force" -> force = argFlag(force, "force", -1);
			case "--hex" -> hex = argFlag(hex, "hex", 1);
			case "--no-hex" -> hex = argFlag(hex, "hex", -1);
			default -> error("Unknown argument: " + args[i]);
			}
		}
		try (Reader in = inp == null ? new InputStreamReader(System.in, StandardCharsets.UTF_8)
				: Files.newBufferedReader(inp, StandardCharsets.UTF_8)) {
			OutputStream out;
			if (outp == null) {
				out = System.out;
			} else if (force > 0) {
				out = Files.newOutputStream(outp);
			} else {
				out = Files.newOutputStream(outp, StandardOpenOption.CREATE_NEW);
			}
			if (hex > 0) {
				out = new HexOutputStream(out);
			}
			try (BufferedOutputStream bout = new BufferedOutputStream(out)) {
				OrosVmAssembler asm = new OrosVmAssembler(bout, in);
				asm.assemble();
			}
		}
	}
	
	private static void error(String msg) {
		System.err.println(msg);
		System.err.println("use --help for an overview");
		System.exit(1);
	}
	
	private static int argFlag(int old, String name, int val) {
		if (old != 0) error("--" + name + " or --no-" + name + " specified multiple times");
		return val;
	}
	
	private static Path argFile(String[] args, int i, String name, Path out) {
		if (out != null) error("specified multiple " + name + " files!");
		if (args.length <= i) error("trailing --in argument without " + name + " file");
		return Path.of(args[i]);
	}
	
	private static void argLicense() throws IOException {
		try (InputStream in = OVAMain.class.getResourceAsStream("/de/hechler/patrick/oros/vm/asm/LICENSE");
				Reader r = new InputStreamReader(in, StandardCharsets.UTF_8)) {
			char[] buf = new char[1024];
			while (true) {
				int reat = r.read(buf, 0, 1024);
				if (reat == -1) break;
				if (reat != 1024) {
					System.out.print(new String(buf, 0, reat));
				} else {
					System.out.print(buf);
				}
			}
		}
		System.exit(1);
	}
	
	private static void argHelp() {
		System.out.print("""
				OVAMain: Oros Vm Assembler
				Usage OVAMain [OPTIONS...]
				Options:
				  --help
				    print this message and exit
				  --license
				    print the license and exit
				  --in <INPUT-FILE>
				    read the input (assembler code) from INPUT-FILE
				    if none is given stdin is used
				  --out <OUTPUT-FILE>
				    write the output (machine code) to OUTPUT-FILE
				  --force
				    overwrite the output file if it already exists
				  --no-force
				    fail if the output file already exists (this is already default)
				    also fail if --force is specified
				  --hex
				    write the hex code and not the binary to the output file
				  --no-hex
				    write the binary to the output file
				    fail if --hex is specified
				""");
		System.exit(1);
	}
	
}
