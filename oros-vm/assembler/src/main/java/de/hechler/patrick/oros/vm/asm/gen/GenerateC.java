package de.hechler.patrick.oros.vm.asm.gen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hechler.patrick.oros.vm.asm.Flag;
import de.hechler.patrick.oros.vm.asm.Flag.FlagSet;

public class GenerateC {

	private static final String START_PATH = "src/main/antlr4/de/hechler/patrick/oros/vm/asm/antlr/";
	
	public static void main(String[] args) throws IOException {
		generate();
	}
	
	public static void generate() throws IOException {
		Path target = Path.of(START_PATH + "OrosVmAsmGrammar.g4");
		Path readme = Path.of("../README.md");
		if (!Files.isRegularFile(readme)) {
			throw new NoSuchFileException(readme.toString(), null, "the README must be a regular file!");
		}
		try (BufferedWriter out = Files.newBufferedWriter(target, StandardCharsets.UTF_8)) {
			try (BufferedReader in = Files.newBufferedReader(readme, StandardCharsets.UTF_8)) {
				generate(out, in);
			}
		}
	}
	
	private static final Pattern AVAIL_CMDS       = Pattern.compile("^###\\s*Available\\s+Commands$",
			Pattern.CASE_INSENSITIVE);
	private static final Pattern CMD_GROUP_HEADER = Pattern.compile("^####\\s+([^\r\n]*)$", Pattern.CASE_INSENSITIVE);
	
	private static final Pattern CMD_NAME = Pattern.compile(
			"^\\+ `(?<cmd>[A-Z]+)( (?<p0>PARAM|CONST|ADDRESS)( (?<p1>PARAM|CONST|ADDRESS))?)?`$",
			Pattern.CASE_INSENSITIVE);

	private static final Pattern MD_CODE = Pattern.compile("`([^`\r\n]+)`", Pattern.CASE_INSENSITIVE);

	private static final Pattern BEHAVIOR = Pattern.compile("^(    ){2,3}[0-9]+[.] .*$", Pattern.CASE_INSENSITIVE);
	
	private static void generate(BufferedWriter out, BufferedReader in) throws IOException {
		while (true) {
			String line = in.readLine();
			if (AVAIL_CMDS.matcher(line).matches()) break;
		}
		boolean beforeStart = true;
		Set<FlagSet> possibleFlags = new HashSet<>();
		List<String> possibleCommands = new ArrayList<>();
		while (true) {
			String line = in.readLine();
			if (line == null) break;
			if (line.isEmpty()) continue;
			Matcher matcher = CMD_GROUP_HEADER.matcher(line);
			if (matcher.matches()) {
				beforeStart = false;
				continue;
			}
			matcher = CMD_NAME.matcher(line);
			if (!matcher.matches()) {
				if (beforeStart && !BEHAVIOR.matcher(line).matches()) {
					throw new AssertionError(line);
				}
				continue;
			}
			String cmdName = matcher.group("cmd");
			String cmdP0 = matcher.group("p0");
			String cmdP1 = matcher.group("p1");
			line = in.readLine();
			if (!"    + description:".equals(line)) {
				throw new AssertionError(line);
			}
			line = in.readLine();
			if (!line.startsWith("        + ")) {
				throw new AssertionError(line);
			}
			while (true) {
				line = in.readLine();
				if (!line.startsWith("        + ")) {
					break;
				}
			}
			FlagSet flags = new FlagSet();
			while (line.startsWith("    + flags")) {
				matcher = MD_CODE.matcher(line);
				while (matcher.find()) {
					flags.addAll(Flag.F.get(matcher.group(1)));
				}
				line = in.readLine();
			}
			if (!line.startsWith("    + value:")) {
				throw new AssertionError(line);
			}
			matcher = MD_CODE.matcher(line);
			if (!matcher.find()) {
				throw new AssertionError(line);
			}
			String value = matcher.group(1);
			line = in.readLine();
			if (!"    + behavior:".equals(line)) {
				throw new AssertionError(line);
			}
			generateCmd(out, cmdName, cmdP0, cmdP1, flags, value);
			possibleFlags.add(flags);
			possibleCommands.add(cmdName);
		}
		for (FlagSet flags : possibleFlags) {
			generatePosFlags(out, flags);
		}
		generatePosCmds(out, possibleCommands);
		generateFlagToks(out);
	}
	
	private static void generateFlagToks(BufferedWriter out) throws IOException {
		out.write('\n');
		for (Flag f : Flag.values()) {
			out.write(f.name());
			out.write(": '");
			out.write(f.name().substring("FLAG_".length()));
			out.write("-';\n");
		}
		out.write('\n');
	}
	
	private static void generatePosFlags(BufferedWriter out, FlagSet flags) throws IOException {
		out.write('\n');
		parseFlagsName(out, flags);
		out.write("\n  returns [int flags, Set<Flag> flagsSet]");
		out.write("\n:\n\t|");
		boolean first = true;
		for (Flag flag : flags) {
			out.write("\n\t");
			if (first) {
				first = false;
			} else {
				out.write("| ");
			}
			out.write(flag.name());
			out.write("\n\t{$flags = ");
			out.write(Integer.toString(flag.value));
			out.write(";\n"//
					+ "\t\t$flagsSet = new Flag.FlagSet(");
			out.write(Integer.toString(1 << flag.ordinal()));
			out.write(");\n"//
					+ "\t}");
		}
		if (flags.contains(Flag.FLAG_U)) {
			for (Flag flag : flags) {
				if (flag == Flag.FLAG_U || flag == Flag.FLAG_FP) continue;
				out.write("\n\t| (\n"//
						+ "\t\tFLAG_U ");
				out.write(flag.name());
				out.write("\n\t\t| ");
				out.write(flag.name());
				out.write(" FLAG_U\n"//
						+ "\t) {\n"//
						+ "\t\t$flags = ");
				out.write(Integer.toString(flag.value | Flag.FLAG_U.value));
				out.write(";\n"//
						+ "\t\t$flagsSet = new Flag.FlagSet(");
				out.write(Integer.toString(( 1 << flag.ordinal() ) | ( 1 << Flag.FLAG_U.value )));
				out.write(");\n"//
						+ "\t}");
			}
		}
		out.write("\n;\n");
	}
	
	private static void parseFlagsName(BufferedWriter out, FlagSet flags) throws IOException {
		out.write("parse_flags");
		for (Flag flag : flags) {
			out.write('_');
			out.write(flag.toString());
		}
	}
	
	private static void generatePosCmds(BufferedWriter out, List<String> possibleCommands) throws IOException {
		out.write("\ncommand"//
				+ "\n  returns [OrosVmCommand cmd]"//
				+ "\n:");
		boolean first = true;
		for (String cmd : possibleCommands) {
			out.write("\n\t");
			if (first) {
				first = false;
			} else {
				out.write("| ");
			}
			out.write("command_");
			out.write(cmd);
			out.write("\n\t{$cmd = $command_");
			out.write(cmd);
			out.write(".cmd;}");
		}
		out.write("\n;\n");
		for (String cmd : possibleCommands) {
			out.write("\nCMD_");
			out.write(cmd);
			out.write(": '");
			out.write(cmd);
			out.write("';");
		}
		out.write('\n');
	}
	
	private static void generateCmd(BufferedWriter out, String cmdName, String cmdP0, String cmdP1, FlagSet flags,
			String value) throws IOException {
		out.write("\ncommand_");
		out.write(cmdName);
		out.write("\n  returns [OrosVmCommand cmd]"//
				+ "\n@init {"//
				+ "\n\tOrosVmParameter p0 = null;"//
				+ "\n\tOrosVmParameter p1 = null;"//
				+ "\n}"//
				+ "\n:\n\t");
		parseFlagsName(out, flags);
		out.write(" CMD_");
		out.write(cmdName);
		if (cmdP0 != null) {
			out.write(" param_");
			out.write(cmdP0);
			out.write(" [$");
			parseFlagsName(out, flags);
			out.write(".flagsSet]\n"//
					+ "\t{p0 = $param_");
			out.write(cmdP0);
			out.write(".param;}");
			if (cmdP1 != null) {
				out.write("\n\tparam_");
				out.write(cmdP1);
				out.write(" [$");
				parseFlagsName(out, flags);
				out.write(".flagsSet]\n"//
						+ "\t{p1 = $param_");
				out.write(cmdP1);
				out.write(".param;}");
			}
		}
		if (value.length() != 4 || value.charAt(3) != '~') {
			throw new AssertionError(value);
		}
		out.write("\n\t{$cmd = new OrosVmCommand($");
		parseFlagsName(out, flags);
		out.write(".flags | 0x");
		out.write(value.substring(0, 3));
		out.write("0, p0, p1);}"//
				+ "\n;\n");
	}
	
}
