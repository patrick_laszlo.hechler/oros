// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;

import de.hechler.patrick.oros.vm.asm.antlr.OrosVmAsmGrammarLexer;
import de.hechler.patrick.oros.vm.asm.antlr.OrosVmAsmGrammarParser;
import de.hechler.patrick.oros.vm.asm.antlr.OrosVmAsmGrammarParser.FileContext;

public class OrosVmAssembler {
	
	private final OutputStream out;
	private final Reader       in;
	
	public OrosVmAssembler(OutputStream out, InputStream in) {
		this(out, new InputStreamReader(in, StandardCharsets.UTF_8));
	}
	
	public OrosVmAssembler(OutputStream out, Reader in) {
		this.out = out;
		this.in = in;
	}
	
	public void assemble() throws IOException {
		FileContext file = parseAsm();
		Map<String, Long> labels = fillLabels(file, new HashMap<>());
		byte[] buf = new byte[1024];
		int off = 0;
		long offset = 0L;
		for (int i = 0; i < file.commands.size(); i++) {
			OrosVmCommand cmd = file.commands.get(i);
			if (buf.length - off < 2) {
				out.write(buf, 0, off);
				offset += off;
				off = 0;
			}
			buf[off] = (byte) cmd.cmdValue;
			buf[off + 1] = (byte) ( cmd.cmdValue >>> 8 );
			off += 2;
			if (cmd.p1 == null) continue;
			OrosVmParam p = cmd.p1.toParam(file.commands, i, offset + off, labels);
			int len = p.dataLength();
			if (buf.length - off < len) {
				out.write(buf, 0, off);
				offset += off;
				off = 0;
			}
			p.data(buf, off);
			off += len;
			if (cmd.p2 == null) continue;
			p = cmd.p2.toParam(file.commands, i, offset + off, labels);
			len = p.dataLength();
			if (buf.length - off < len) {
				out.write(buf, 0, off);
				offset += off;
				off = 0;
			}
			p.data(buf, off);
			off += len;
		}
		out.write(buf, 0, off);
	}
	
	private static Map<String, Long> fillLabels(FileContext file, Map<String, Range> labels) throws AssertionError {
		Range lastRange = null;
		boolean define = false;
		while (true) {
			Range offset = new Range(0L, 0L);
			for (int i = 0; i < file.commands.size(); i++) {
				OrosVmCommand cmd = file.commands.get(i);
				for (String l : cmd.labels) {
					Range old = labels.put(l, new Range(offset));
					if (old != null && lastRange == null) {
						throw new IllegalStateException("encountered label '" + l + "' multiple times!");
					}
				}
				if (cmd.p1 instanceof OrosVmParam p1) {
					addKnownParamLength(offset, cmd, p1);
				} else if (cmd.p1 instanceof OrosVmParamLabel l) {
					addLabelParamLength(labels, offset, l, define);
				} else if (cmd.p1 != null) {
					throw new AssertionError("unknown param type: " + cmd.p1.getClass());
				}
				System.out.println(cmd);
			}
			if (offset.min == offset.max) {
				@SuppressWarnings("rawtypes")
				Map map = labels;
				@SuppressWarnings("unchecked")
				Map<String, Object> so = map;
				so.replaceAll((k, old) -> {
					Range o = (Range) old;
					if (o.min != o.max) throw new AssertionError();
					return o.min;
				});
				@SuppressWarnings("unchecked")
				Map<String, Long> sl = map;
				return sl;
			}
			System.out.println(offset.min + ".." + offset.max);
			if (lastRange != null && offset.min == lastRange.min && offset.max == lastRange.min) {
				define = true;
			}
			lastRange = offset;
		}
	}
	
	private static void addLabelParamLength(Map<String, Range> labels, Range offset, OrosVmParamLabel l,
			boolean define) {
		Set<Flag> f = l.flags();
		if (f.contains(Flag.FLAG_8)) {
			offset.min += 3;
			offset.max += 3;
		} else if (f.contains(Flag.FLAG_16)) {
			offset.min += 4;
			offset.max += 4;
		} else if (f.contains(Flag.FLAG_32)) {
			offset.min += 6;
			offset.max += 6;
		} else if (f.contains(Flag.FLAG_64)) {
			offset.min += 10;
			offset.max += 10;
		} else {
			Range target = labels.get(l.name());
			if (target == null) {
				offset.min += 3;
				offset.max += 10;
			} else {
				long minDiff;
				long maxDiff;
				if (offset.min > target.max) {
					minDiff = offset.min - target.max;
					maxDiff = offset.max - target.min;
				} else {
					minDiff = target.max - offset.min;
					maxDiff = target.min - offset.max;
				}
				if (define) {
					minDiff = maxDiff;
				}
				int minDiffSize = jmpSize(minDiff);
				int maxDiffSize = jmpSize(maxDiff);
				if (minDiffSize == maxDiffSize) {
					switch (minDiffSize) {
					case 1 -> l.flags().add(Flag.FLAG_8);
					case 2 -> l.flags().add(Flag.FLAG_16);
					case 4 -> l.flags().add(Flag.FLAG_32);
					case 8 -> l.flags().add(Flag.FLAG_64);
					default -> throw new AssertionError(minDiffSize);
					}
				}
				offset.min += 2 + minDiffSize;
				offset.max += 2 + maxDiffSize;
			}
		}
	}
	
	private static void addKnownParamLength(Range offset, OrosVmCommand cmd, OrosVmParam p1) throws AssertionError {
		offset.min += 2 + p1.dataLength();
		offset.max += 2 + p1.dataLength();
		if (cmd.p2 instanceof OrosVmParam p2) {
			offset.min += p2.dataLength();
			offset.max += p2.dataLength();
		} else if (cmd.p2 != null) {
			throw new AssertionError("unknown param type: " + cmd.p2.getClass());
		}
	}
	
	private static int jmpSize(long diff) {
		if ((byte) diff == diff) {
			return 1;
		} else if ((short) diff == diff) {
			return 2;
		} else if ((int) diff == diff) {
			return 4;
		} else {
			return 8;
		}
	}
	
	private FileContext parseAsm() throws IOException {
		try {
			ANTLRInputStream ain = new ANTLRInputStream(in);
			OrosVmAsmGrammarLexer lexer = new OrosVmAsmGrammarLexer(ain);
			CommonTokenStream toks = new CommonTokenStream(lexer);
			OrosVmAsmGrammarParser parser = new OrosVmAsmGrammarParser(toks);
			parser.setErrorHandler(new BailErrorStrategy());
			return parser.file();
		} finally {
			in.close();
		}
	}
	
}
