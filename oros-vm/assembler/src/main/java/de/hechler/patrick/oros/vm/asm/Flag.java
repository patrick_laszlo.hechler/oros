// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

public enum Flag {
	FLAG_FP(4),
	
	FLAG_U(8),
	
	FLAG_8(1), FLAG_16(2), FLAG_32(3),
	
	FLAG_64(0),
	
	FLAG_128(5), FLAG_256(6), FLAG_512(7),
	
	;
	
	public final int value;
	
	private Flag(int value) {
		this.value = value;
	}
	
	public static final Map<String, FlagSet> F;
	
	static {
		Map<String, FlagSet> f = new HashMap<>();
		f.put("FP", new FlagSet(1 << FLAG_FP.ordinal()));
		f.put("U", new FlagSet(1 << FLAG_U.ordinal()));
		f.put("N", new FlagSet(( 1 << FLAG_8.ordinal() ) | ( 1 << FLAG_16.ordinal() ) | ( 1 << FLAG_32.ordinal() )));
		f.put("BIG_N",
				new FlagSet(( 1 << FLAG_128.ordinal() ) | ( 1 << FLAG_256.ordinal() ) | ( 1 << FLAG_512.ordinal() )));
		f.put("8", new FlagSet(1 << FLAG_8.ordinal()));
		f.put("16", new FlagSet(1 << FLAG_16.ordinal()));
		f.put("32", new FlagSet(1 << FLAG_32.ordinal()));
		f.put("64", new FlagSet(1 << FLAG_64.ordinal()));
		f.put("128", new FlagSet(1 << FLAG_128.ordinal()));
		f.put("256", new FlagSet(1 << FLAG_256.ordinal()));
		f.put("512", new FlagSet(1 << FLAG_512.ordinal()));
		F = Map.copyOf(f);
	}
	
	public static class FlagSet extends AbstractSet<Flag> {
		
		private int vals;
		
		public FlagSet(int vals) {
			this.vals = vals;
		}
		
		public FlagSet() {}
		
		public boolean contains(Flag f) {
			return ( vals & ( 1 << f.ordinal() ) ) != 0;
		}
		
		@Override
		public boolean add(Flag f) {
			int mask = 1 << f.ordinal();
			if (( vals & mask ) == 0) {
				vals |= mask;
				return true;
			}
			return false;
		}
		
		@Override
		public boolean addAll(Collection<? extends Flag> c) {
			if (c instanceof FlagSet fs) {
				if (( vals | fs.vals ) != vals) {
					vals |= fs.vals;
					return true;
				}
				return false;
			}
			return super.addAll(c);
		}
		
		public void remove(Flag f) {
			vals &= ~( 1 << f.ordinal() );
		}
		
		@Override
		public boolean remove(Object o) {
			if (o instanceof Flag f) {
				int mask = 1 << f.ordinal();
				if (( vals & mask ) != 0) {
					vals &= ~mask;
					return true;
				}
			}
			return false;
		}
		
		@Override
		public int size() {
			return Integer.bitCount(vals);
		}
		
		@Override
		public boolean equals(Object o) {
			return super.equals(o);
		}
		
		@Override
		public Iterator<Flag> iterator() {
			return new Iterator<>() {
				private static final Flag[] FLAGS = Flag.values();
				private int                 o     = 0;
				
				@Override
				public Flag next() {
					for (; o < FLAGS.length; o++) {
						if (( vals & ( 1 << o ) ) != 0) {
							return FLAGS[o++];
						}
					}
					throw new NoSuchElementException("no more flags");
				}
				
				@Override
				public boolean hasNext() {
					for (; o < FLAGS.length; o++) {
						if (( vals & ( 1 << o ) ) != 0) {
							return true;
						}
					}
					return false;
				}
			};
		}
	}
}