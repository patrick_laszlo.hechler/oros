// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

public class Range {
	
	public long min;
	public long max;
	
	public Range(long min, long max) {
		this.min = min;
		this.max = max;
	}
	
	public Range(Range copy) {
		this.min = copy.min;
		this.max = copy.max;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) ( this.max ^ ( this.max >>> 32 ) );
		result = prime * result + (int) ( this.min ^ ( this.min >>> 32 ) );
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!( obj instanceof Range )) {
			return false;
		}
		Range other = (Range) obj;
		if (this.max != other.max) {
			return false;
		}
		if (this.min != other.min) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Range [");
		builder.append(this.min);
		builder.append("..");
		builder.append(this.max);
		builder.append(']');
		return builder.toString();
	}
	
}
