// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.asm;

import java.util.List;

public class OrosVmCommand {
	
	public final int             cmdValue;
	public final OrosVmParameter p1;
	public final OrosVmParameter p2;
	public final List<String>    labels;
	
	public OrosVmCommand(int cmdValue, OrosVmParameter p1, OrosVmParameter p2) {
		this.cmdValue = cmdValue;
		this.p1 = p1;
		this.p2 = p2;
		this.labels = List.of();
	}
	
	public OrosVmCommand(OrosVmCommand cmd, List<String> labels) {
		this.cmdValue = cmd.cmdValue;
		this.p1 = cmd.p1;
		this.p2 = cmd.p2;
		this.labels = List.copyOf(labels);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String l : labels) {
			sb.append(l).append(": ");
		}
		String cmd = Integer.toHexString(cmdValue).toUpperCase();
		if (cmd.length() != 4) {
			sb.append("0000", 0, 4 - cmd.length());
		}
		sb.append(cmd);
		if (p1 != null) sb.append(' ').append(p1);
		if (p1 instanceof OrosVmParam p) sb.append('=').append(toString(p.data()));
		if (p2 != null) sb.append(' ').append(p2);
		if (p2 instanceof OrosVmParam p) sb.append('=').append(toString(p.data()));
		return sb.toString();
	}
	
	private static char[] toString(byte[] data) {
		if (data.length == 0) return "[]".toCharArray();
		char[] res = new char[data.length << 2];
		res[0] = '[';
		for (int i = 0, ri = 1;; i++, ri += 4) {
			res[ri] = toHex(data[i] >>> 4);
			res[ri + 1] = toHex(data[i] & 0xF);
			if (ri + 3 == res.length) {
				res[ri + 2] = ']';
				break;
			}
			res[ri + 2] = ',';
			res[ri + 3] = ' ';
		}
		return res;
	}
	
	private static char toHex(int val) {
		if (val < 10) return (char) ( val + '0' );
		else return (char) ( val + ( 'A' - 10 ) );
	}
	
}
