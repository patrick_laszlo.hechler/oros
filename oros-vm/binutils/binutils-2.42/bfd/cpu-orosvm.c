/* BFD support for the OrosVm architecture
   Copyright (C) 2017-2024 Free Software Foundation, Inc.
   Contributed by Patrick Hechler <binutils.orosvm@ph.anderemails.de>

   This file is part of BFD, the Binary File Descriptor library.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor,
   Boston, MA 02110-1301, USA.  */

#include "sysdep.h"
#include "bfd.h"
#include "libbfd.h"

const bfd_arch_info_type bfd_orosvm_arch =
{
  /* word size in bits */
  64,

  /* address size in bits */
  64,

  /* byte size in bits */
  8,

  /* architecture enum */
  bfd_arch_orosvm,

  /* machine number */
  bfd_mach_orosvm,

  /* architecture name */
  "orosvm",

  /* printable name */
  "orosvm",

  /* alignment */
  1,

  /* is default ? */
  true,

  /* default checker for architecture compatibility */
  bfd_default_compatible,

  /* if a particular bfd_arch_info_type matches
     "this" bfd_arch_info_type */
  bfd_default_scan,

  /* memory allocator around bfd_malloc */
  bfd_arch_default_fill,

  /* pointer to the next bfd_arch_info_type structure
     only needed if multiple architectures are defined */
  NULL
};
