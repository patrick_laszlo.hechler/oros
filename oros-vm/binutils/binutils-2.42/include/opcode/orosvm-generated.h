// TODO generate this file automatically

// DO NOT INCLUDE THIS HEADER MANUALLY
#ifndef _OROSVM_OPCODES_H
#error "DO NOT INCLUDE THIS HEADER! include 'opcode/orosvm.h' instead"
#endif /* _OROSVM_OPCODES_H */

enum orosvm_opcode {
  OP_NOP = 0x0000,
  OP_JMP = 0x0001,
  OP_MOV = 0x0002,
  OP_ADD = 0x0003,
};

