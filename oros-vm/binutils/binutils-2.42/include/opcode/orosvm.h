/* Opcode table for the Oros Virtual Mashine.

   Copyright (C) 2000-2024 Free Software Foundation, Inc.
   Contributed by Patrick Hechler <binutils.orosvm@ph.anderemails.de>
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.  */

#ifndef _OROSVM_OPCODES_H
#define _OROSVM_OPCODES_H

/* probably define OROSVM_BYTES_SLOT_INSTRUCTION
   and OROSVM_BYTES_SLOT_IMMEDIATE */

#include "orosvm-generated.h"

enum orosvm_admode {
  AM_0000 = 0, /* constant value / immediate */
  AM_0001 = 1, /* general usage register */
  AM_0111 = 7, /* dereference general usage register */
};

typedef struct
{
  enum orosvm_admode admode;
  expressionS *exp;
  expressionS *exp2;
} param;

typedef struct
{
  int opcode; /* opcode (including flags) 16 bit */
  int p_cnt;
  param p[2];
} orosvm_slot_insn;

#endif /*_OROSVM_OPCODES_H */

