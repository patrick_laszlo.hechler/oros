/* tc-orosvm.h -- Header file for tc-orosvm.c.
   Copyright (C) 2009-2024 Free Software Foundation, Inc.
   Contributed by Patrick Hechler.

   This file is part of GAS.

   GAS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the license, or
   (at your option) any later version.

   GAS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING3. If not,
   see <http://www.gnu.org/licenses/>.  */

#ifndef TC_OROSVM_H
#define TC_OROSVM_H

#define TARGET_FORMAT "elf64-orosvm"
#define TARGET_ARCH bfd_arch_orosvm
#define TARGET_MACH bfd_mach_orosvm
#define TARGET_BYTES_BIG_ENDIAN 0 /* little endian support only */

/* customizable handling of expression operands */
#define md_operand(X)

/* proper byte/char-wise emission of numeric data
   with respect to the endianness */
#define md_number_to_chars number_to_chars_littleendian

/* not having this will require more hooks
   to handle such as md_create_long_jump,
   md_long_jump_size. ...
   ok for now */
#define WORKING_DOT_WORD

#endif /* TC_OROSVM_H */
