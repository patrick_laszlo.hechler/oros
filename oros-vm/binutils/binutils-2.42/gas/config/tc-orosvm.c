/* tc-orosvm.c -- Assemble for the OrosVm ISA

   Copyright (C) 2009-2024 Free Software Foundation, Inc.
   Contributed by Patrick Hechler.

   This file is part of GAS.

   GAS is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the license, or
   (at your option) any later version.

   GAS is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING3. If not,
   see <http://www.gnu.org/licenses/>.  */

/* absolute minimal OrosVm assembler setup, this is not yet finish, TODO */

#include "as.h"
#include "bfd.h"
#include "opcode/orosvm.h"

const char *md_shortopts = "";
struct option md_longopts[] = {};
size_t md_longopts_size = ARRAY_SIZE(md_longopts);

/* comment characters and separators */
const char comment_chars[] = "";
const char line_comment_chars[] = "//";
const char line_separator_chars[] = ";";

/* no floating point (yet TODO) */
const char EXP_CHARS[] = "";
const char FLT_CHARS[] = "";

/* no pseudo ops */
const pseudo_typeS md_pseudo_table[] =
{
  {(char*)0, (void(*)(int))0, 0}
};

static htab_t reg_table;

hashval_t reg_table_hash(const void *e)
{
  symbolS *sym = e;
  return S_GET_VALUE(sym);
}
int reg_table_eq(const void *a, const void *b)
{
  symbolS *syma = a, *symb = b;
  return S_GET_VALUE(syma) == S_GET_VALUE(symb);
}
void reg_table_del(void *e)
{
  symbolS *sym = e;
  as_fatal(_("delete element from register table"));
  symbol_mark_removed(sym);
}

/* there are no options to parse */
int md_parse_option(int c, const char *arg)
{
  return 0;
}

/* show command line usage */
void md_show_usage(FILE *stream)
{
  fputs("OrosVm options: there are none\n", stream);
}

/*****************************************************************************/
/* See if the specified name is a register (r0-rF). Nothing actually to      */
/* parse here, just look up in the register table                            */
/*****************************************************************************/
static int orosvm_parse_register(const char* name, expressionS *resultP)
{
  symbolS *reg_sym = htab_find(reg_table, name);
  if (reg_sym != 0)
  {
    resultP->X_op = O_register;
    resultP->X_add_number = S_GET_VALUE(reg_sym);
    return 1;
  }
  return 0;
}

static int orosvm_parse_symbol(const char *name, expressionS *resultP, char *next_char)
{
  symbolS *sym = symbol_find_or_make(name);
  know(sym != 0);
  resultP->X_op = O_symbol;
  resultP->X_add_symbol = sym;
  /* this is kind of a hack. A dummy balue to avoid 'make_expr_symbol'
     turn the node into 'O_constant' */
  resultP->X_add_number = 1;
  return 1;
}

/*****************************************************************************/
/* In order to catch assembler keywords assign them properly, hook in a      */
/* custom name checking routine                                              */
/*****************************************************************************/
int orosvm_parse_name(const char *name, expressionS *resultP, char *next_char)
{
  gas_assert(name != 0 && resultP != 0);

  if (orosvm_parse_register(name, resultP) != 0)
      return 1;

  if (orosvm_parse_symbol(name, resultP, next_char) != 0)
      return 1;

  return 0;
}

static void orosvm_add_reg(const char *name, int i)
{
  symbolS *reg_sym = symbol_new(name, reg_section, &zero_address_frag, i);
  /* we insert registers as 'symbolS' with no correspondence
   to a 'bfd_symbol' */
  if (htab_insert(reg_table, reg_sym, 0))
    {
      as_fatal(_("failed to create register symbols"));
    }
}

/*****************************************************************************/
/* Do any architecture specific initialization, we use this to set up the    */
/* table of registers that we treat as already known 'symbols'               */
/*****************************************************************************/
void md_begin(void)
{
  reg_table = htab_create(0x12, reg_table_hash, reg_table_eq, reg_table_del);
  for (int i = 0; i <= 0xF; ++i)
    {
      char name[4];
      sprintf(name, "r%X", i);
      orosvm_add_reg(name, i);
    }
  orosvm_add_reg("ip", 0x10);
  orosvm_add_reg("sp", 0x11);
}

void md_assemble(char *insn_str)
{
  printf("assemble now the instruction '%s'\n", insn_str);

  orosvm_slot_insn *insn = 0;
  expressionS p0_info;
  expressionS p1_info;
}

/* this gets invoked in case 'md_parse_name' has failed */
symbolS* md_undefined_symbol(char *name)
{
  as_bad("undefined symbol '%s'\n", name);
  return 0;
}

/* no floating point expressions available yet */
const char* md_atof(int type, char *lit, int *size)
{
  return 0;
}

/* can't do that yet */
valueT md_section_align(asection *seg, valueT val)
{
  return 0;
}

/* no support for frag conversion */
void md_convert_frag(bfd *abfd, asection *seg, fragS *fragp)
{
  as_fatal(_("unexpected call"));
}

/* nope */
void md_apply_fix(fixS *fixp, valueT *val, segT seg)
{
}

/* if not applied/resolved, turn a fixup into a relocation entry */
arelent* tc_gen_reloc(asection *seg, fixS *fixp)
{
  return 0;
}

/* no pc-relative relocations yet, might come later */
long md_pcrel_from(fixS *fixp)
{
  as_fatal(_("unexpected call"));
  return 0;
}

int md_estimate_size_before_relax(fragS *fragp, asection *seg)
{
  as_fatal(_("unexpected call"));
  return 0;
}

























