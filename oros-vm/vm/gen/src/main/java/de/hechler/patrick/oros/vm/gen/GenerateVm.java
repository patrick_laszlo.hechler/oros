// SPDX-License-Identifier: AGPL-3.0-or-later
package de.hechler.patrick.oros.vm.gen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenerateVm {
	
	private static final String SRC_PATH = "../src/";
	
	public static void main(String[] args) throws IOException {
		generate();
	}
	
	public static void generate() throws IOException {
	}
	
	private static void delegate(BufferedWriter out, BufferedReader in) throws IOException {
		char[] buf = new char[1024];
		while (true) {
			int len = in.read(buf, 0, 1024);
			if (len == -1) break;
			out.write(buf, 0, len);
		}
	}
	
}
