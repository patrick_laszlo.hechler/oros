#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later

set -e

VERSION="$1"

if test "$VERSION" = "snap"; then
  if test $# != 2; then
    echo "Usage: $(basename $0) [snap] <NEW_VERSION>" >&2
    exit 1
  fi
  VERSION="$2-SNAPSHOT"  
elif test $# != 1; then
  echo "Usage: $(basename $0) [snap] <NEW_VERSION>" >&2
  exit 1
fi

if test "$(echo -n "$VERSION" | wc -l)" != 0; then
  echo "multi line version"
  exit 1
elif test "$(echo -n "$VERSION" | wc -w)" != 1; then
  echo "multi word (or empty) version"
  exit 1
fi


function follow_links {
  FL_PATH="$1"
  while true ; do
    if test -L "$FL_PATH"; then
      LINK="$(readlink -n "$FL_PATH")"
      if [[ "$LINK" =~ '^/.*$'  ]] ; then
        FL_PATH="$(dirname $FL_PATH)/$LINK"
      else
        FL_PATH="$LINK"
      fi
    elif test ! -e "$FL_PATH"; then
      echo "  is a link" >&2
      return 1
    else
      break
    fi
  done
  echo -n "$FL_PATH"
}

function parent_dir_follow_links {
  DIR="$(follow_links "$1")"
  DIR="$(dirname "$DIR")"
  DIR="$(follow_links "$DIR")"
  echo -n "$DIR"
}

WD="$(parent_dir_follow_links "$0")"

cd "$WD"

echo "set version to $VERSION"
echo "set version in these files:"
echo "  VERSION"
echo -n $VERSION > VERSION

find . -name "pom.xml" \
  -a -exec echo '  {}' ';' \
  -a -exec sed -E 's#^( {,8}<version>)[a-zA-Z0-9._-]+(</version>)#\1'$VERSION'\2#' --in-place '{}' ';'
